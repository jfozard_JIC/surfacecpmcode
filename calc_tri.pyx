
import cython

from libc.math cimport sqrt, ldexp

cdef double dist(double[2] a, double[2] b):
    return sqrt((a[0]-b[0])**2 + (a[1]-b[1])**2)

cdef double d_centroids(double[3][2] pp1, double[3][2] pp2):
    cdef double c1[3]
    cdef double c2[3]
    cdef int i
    for i in range(2):
        c1[i] = (pp1[0][i] + pp1[1][i] + pp1[2][i])/3.0
    for i in range(2):
        c2[i] = (pp2[0][i] + pp2[1][i] + pp2[2][i])/3.0
    return dist(c1, c2)

cdef double d_max(double[3][2] pp1, double[3][2] pp2):
    cdef double d = 0.0
    cdef double r
    cdef int i, j
    for i in range(3):
        for j in range(3):
            r = dist(pp1[i], pp2[j])
            if r>d:
                d = r
    return d
    

cdef double calc_tri(double[3][2] pp1, double[3][2] pp2, double r, int l):
    if l==0:
        if d_centroids(pp1, pp2)<r:
            return 1.0
        else:
            return 0.0

    if d_max(pp1, pp2)<r:
        return 1.0

    cdef double new_pp1[3][2]
    """
    new_tris = [ [pp1[0], mid[2], mid[1]],
                 [pp1[1], mid[0], mid[2]],
                 [pp1[2], mid[1], mid[0]],
                 [mid[0], mid[1], mid[2]] ]
    """
    cdef double tot = 0.0
    cdef int i
    
    new_pp1[0][:] = pp1[0][:]
    for i in range(2):
        new_pp1[1][i] = 0.5*(pp1[0][i]+pp1[1][i])
    for i in range(2):
        new_pp1[2][i] = 0.5*(pp1[0][i]+pp1[2][i])
    tot += 0.25*calc_tri(pp2, new_pp1, r, l-1)

    new_pp1[0][:] = pp1[1][:]
    for i in range(2):
        new_pp1[1][i] = 0.5*(pp1[2][i]+pp1[1][i])
    for i in range(2):
        new_pp1[2][i] = 0.5*(pp1[0][i]+pp1[1][i])
    tot += 0.25*calc_tri(pp2, new_pp1, r, l-1)

    new_pp1[0][:] = pp1[2][:]
    for i in range(2):
        new_pp1[1][i] = 0.5*(pp1[0][i]+pp1[2][i])
    for i in range(2):
        new_pp1[2][i] = 0.5*(pp1[1][i]+pp1[2][i])
    tot += 0.25*calc_tri(pp2, new_pp1, r, l-1)

    for i in range(2):
        new_pp1[0][i] = 0.5*(pp1[2][i]+pp1[1][i])
    for i in range(2):
        new_pp1[1][i] = 0.5*(pp1[0][i]+pp1[2][i])
    for i in range(2):
        new_pp1[2][i] = 0.5*(pp1[1][i]+pp1[0][i])
    tot += 0.25*calc_tri(pp2, new_pp1, r, l-1)
    
    return tot

def calc_tri_a(double[:,:] pp1, double[:,:] pp2, r, max_level=2):
    return calc_tri(<double (*)[2]>(&pp1[0,0]), <double (*)[2]>(&pp2[0,0]), r, max_level)
