

import numpy as np
cimport numpy as np
import cython
cimport cython
cimport cpython

from libc.stdlib cimport rand, RAND_MAX
from libc.math cimport sqrt, ldexp, exp

from random import randint, random

cdef double EPSILON = 1e-16

cdef extern unsigned int pcg32_random()
cdef extern void pcg32_srandom(unsigned long, unsigned long)
cdef extern unsigned int pcg32_boundedrand(unsigned int)

pcg32_srandom(32, 48)

@cython.boundscheck(False)
@cython.wraparound(False)
@cython.nonecheck(False)
@cython.cdivision(True)
def evolve_CPM(cpm, int n_steps):

    cdef int[:,:] state = cpm.state
    cdef float[:] _area = cpm._area
    cdef float[:] _perimeter = cpm._perimeters


    cdef int nrow = state.shape[0]
    cdef int ncol = state.shape[1]
    cdef int np = nrow*ncol


    cdef double[:,:] J = cpm.J

    cdef double[:] target_perimeter = cpm.target_perimeter
    cdef double[:] target_area = cpm.target_area

    cdef int[:] ep_i = cpm.mesh.ep_i
    cdef int[:] ep_j = cpm.mesh.ep_j
    cdef float[:] ep_w = cpm.mesh.ep_w

    cdef int n_ep = ep_i.shape[0]

    cdef int[:] connectivity_i= cpm.mesh.connectivity_i
    cdef int[:] connectivity_j = cpm.mesh.connectivity_j
    cdef float[:] connectivity_w = cpm.mesh.connectivity_w

    cdef int n_c = connectivity_i.shape[0]
    cdef float connectivity_sum = 0.0

    cdef int[:] copy_i = cpm.mesh.copy_i
    cdef int[:] copy_j = cpm.mesh.copy_j

    cdef int n_cp = copy_i.shape[0]
    
    cdef float area = cpm.mesh.area

    cdef int[:] celltype = cpm.celltype

    cdef float va

    cdef int bdd = 0 # Keep track of copy attempts for which source and target have different labels
    cdef int acc = 0 # Keep track of accepted copy attempts
    
    cdef int acc_up = 0 # Keep track of accepted uphill copy attempts

    cdef int i, j, l, k, kk, r, di, dj
    cdef int ni, nj, n2i, n2j, n3i, n3j, sigma, nsigma, n2sigma
    cdef int ct, nct, n2ct
    cdef double dH

    cdef float s, t, n2p


    cdef double lambda_area = cpm.lambda_area
    cdef double lambda_perimeter = cpm.lambda_perimeter #/cpm.xi/cpm.xi

    cdef double dperimeter, dnperimeter, darea, dnarea
    
    cdef double temperature = cpm.temperature

    cdef double total_dH = 0.0
    
    # calculate total connectivity
    for i in range(n_c):
        connectivity_sum += connectivity_w[i]

    for l in range(n_steps):

        r = pcg32_boundedrand(np)
        i = r%nrow
        j = (r/nrow)


# Uniform selection of neighbour from connectivity
#        dir = pcg32_boundedrand(n_c)
#        di = connectivity_i[dir]
#        dj = connectivity_j[dir]
#        ni = (i+di)
#        nj = (j+dj)

        s = connectivity_sum*ldexp(pcg32_random(), -32)
        t = 0.0
        for dir in range(n_c):
            t+=connectivity_w[dir]
            if t>=s:
                break
        else:
            dir = 0
    
        di = connectivity_i[dir]
        dj = connectivity_j[dir]
        ni = (i+di)
        nj = (j+dj)

        # Periodic BCs for source site selection
        if ni < 0:
            ni = nrow + ni
        elif ni >= nrow:
            ni = ni - nrow
        if nj < 0:
            nj = ncol + nj
        elif nj >= ncol:
            nj = nj - ncol

        sigma = state[i, j]
        nsigma = state[ni, nj]
        if sigma != nsigma: # Quick exit if copy does nothing
            ct = celltype[sigma]
            nct = celltype[nsigma]
            
            dperimeter = 0.0
            dnperimeter = 0.0
            darea = 0.0
            dnarea = 0.0
            bdd += 1 
            dH = 0
            
            va = area
            darea += -va
            dnarea += va

            # Calculate boundary / perimeter changes for target site
            for k in range(n_ep):
                n2i = i + ep_i[k]
                # Periodic BCs for neighbour of i
                if n2i < 0:
                    n2i = n2i + nrow
                elif n2i >= nrow:
                    n2i = n2i - nrow
                n2j = j + ep_j[k]
                if n2j < 0:
                    n2j = n2j + ncol
                elif n2j >= ncol:
                    n2j = n2j - ncol

                n2p = ep_w[k]
                n2sigma = state[n2i, n2j]
                n2ct = celltype[n2sigma]

                # Check to see if this site is in the copy region
                if n2sigma == sigma:
                    flg = 0
                    # Only have to check copy nhbhood - compare offset from target site
                    for kk in range(n_cp):
                        if ep_i[k] == copy_i[kk] and ep_j[k]==copy_j[kk]:
                            flg = 1
                            break
                    if flg == 1:
                        continue
                        # neighbour will also be switched to nsigma - no contribution
                        # to dH and dperimeter dnperimeter
                        
                
                if sigma != n2sigma:
                    dperimeter += -n2p
                    dH -= J[ct, n2ct]*n2p
                else:
                    dperimeter += n2p

                if nsigma != n2sigma:
                    dnperimeter += n2p
                    dH += J[nct, n2ct]*n2p
                else:
                    dnperimeter -= n2p

            # Neighbours of i in the copy region who have same label sigma (otherwise have to consider many cell area changes)
            for kk in range(n_cp):
                n3i = i + copy_i[kk]
                if n3i < 0:
                    n3i = n3i + nrow
                elif n3i >= nrow:
                    n3i = n3i - nrow
                n3j = j + ep_j[kk]
                if n3j < 0:
                    n3j = n3j + ncol
                elif n3j >= ncol:
                    n3j = n3j - ncol
                if state[n3i, n3j] != sigma:
                    continue

                va = area
                darea += -va
                dnarea += va
                # Perimeter contributions for the neighbours of these points


                for k in range(n_ep):
                    n2i = n3i + ep_i[k]
                    # Periodic BCs for neighbour of i
                    if n2i < 0:
                        n2i = n2i + nrow
                    elif n2i >= nrow:
                        n2i = n2i - nrow
                    n2j = n3j + ep_j[k]
                    if n2j < 0:
                        n2j = n2j + ncol
                    elif n2j >= ncol:
                        n2j = n2j - ncol

                    n2p = ep_w[k]
                    n2sigma = state[n2i, n2j]
                    n2ct = celltype[n2sigma]

                    if n2sigma == sigma:
                        flg = 0
                        # check if nb in copy nbhood - also need to check target site
                        # Check copy nhbhood - compare offset from target site
                        if n2i == i and n2j == j:
                            flg = 1
                        else:
                            for kk in range(n_cp):
                                if ep_i[k] == copy_i[kk] and ep_j[k]==copy_j[kk]:
                                    flg = 1
                                    break
                        if flg == 1:
                            continue



                    if sigma != n2sigma:
                        dperimeter += -n2p
                        dH -= J[ct, n2ct]*n2p
                    else:
                        dperimeter += n2p

                    if nsigma != n2sigma:
                        dnperimeter += n2p
                        dH += J[nct, n2ct]*n2p
                    else:
                        dnperimeter -= n2p


            if sigma>0:
                dH += lambda_perimeter * (dperimeter*dperimeter + 2*dperimeter * (_perimeter[sigma] - target_perimeter[sigma]))
                dH += lambda_area * (darea*darea + 2*darea*(_area[sigma] - target_area[sigma]))
            if nsigma>0:
                dH += lambda_perimeter * (dnperimeter*dnperimeter + 2*dnperimeter * (_perimeter[nsigma] - target_perimeter[nsigma]))
                dH += lambda_area * (dnarea*dnarea + 2*dnarea*(_area[nsigma] - target_area[nsigma]))
                    

#            print '***'
#            print i, ni, sigma, nsigma, va, dperimeter, dnperimeter, dH
#            print _area[sigma], _area[nsigma], _perimeter[sigma], _perimeter[nsigma]
#            print target_area[sigma], target_area[nsigma], target_perimeter[sigma], target_perimeter[nsigma]
            

            # Decide whether to accept copy attempt
#            if dH < chance0 and (dH < chance1 or
#                                 ldexp(pcg32_random(), -32) < copyprob[<int>(dH) - chance1 - 1]):               

            if dH < 0 or ldexp(pcg32_random(), -32) < exp(-dH/temperature):
#                                  < copyprob[<int>(dH) - chance1 - 1]):               


                # perform copy
#                if sigma>0:


                _area[sigma] += darea
                _perimeter[sigma] += dperimeter
                    
#                if nsigma>0:
                _area[nsigma] += dnarea
                _perimeter[nsigma] += dnperimeter

                state[i, j] = nsigma
# Do extended copy
                for kk in range(n_cp):
                    n3i = i + copy_i[k]
                    if n3i < 0:
                        n3i = n3i + nrow
                    elif n3i >= nrow:
                        n3i = n3i - nrow
                    n3j = j + ep_j[k]
                    if n3j < 0:
                        n3j = n3j + ncol
                    elif n3j >= ncol:
                        n3j = n3j - ncol
                    if state[n3i, n3j] == sigma:
                        state[n3i, n3j] = nsigma

                acc += 1
                if dH>0:
                    acc_up += 1
                total_dH += dH
#    print n_steps, bdd, acc
    return acc, bdd, acc_up, total_dH
