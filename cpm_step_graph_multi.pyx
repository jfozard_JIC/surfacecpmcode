

import numpy as np
cimport numpy as np
import cython
cimport cython
cimport cpython

from libc.stdlib cimport rand, RAND_MAX
from libc.math cimport sqrt, ldexp, exp

from random import randint, random

cdef double EPSILON = 1e-16

cdef extern unsigned int pcg32_random()
cdef extern void pcg32_srandom(unsigned long, unsigned long)
cdef extern unsigned int pcg32_boundedrand(unsigned int)

pcg32_srandom(32, 48)

@cython.boundscheck(False)
@cython.wraparound(False)
@cython.nonecheck(False)
@cython.cdivision(True)
cdef int bsearch(double v, double[:] a, int start, int end):
    cdef int l = start
    cdef int r = end
    cdef int m
    if v>=a[end]:
        return end
    while True:
        if l>=r:
            return r
        m = (l+r)/2
        if a[m]>v:
            r = m
        else:
            l = m+1


@cython.boundscheck(False)
@cython.wraparound(False)
@cython.nonecheck(False)
@cython.cdivision(True)
def evolve_CPM(cpm, int n_steps):

    cdef int Nv = len(cpm.state)
    cdef int[:] state = cpm.state
    cdef double[:] _area = cpm._area
    cdef double[:] _perimeter = cpm._perimeters

    cdef double[:,:] J = cpm.J

    cdef double[:] target_perimeter = cpm.target_perimeter
    cdef double[:] target_area = cpm.target_area


    cdef int[:] ep_indices = cpm.mesh.perimeters.indices
    cdef int[:] ep_indptr = cpm.mesh.perimeters.indptr
    cdef double[:] ep = cpm.mesh.perimeters.data
    cdef double[:] ep_sums = cpm.mesh.perimeter_sums

    cdef int[:] indices = cpm.mesh.connectivity.indices
    cdef int[:] indptr = cpm.mesh.connectivity.indptr
    cdef double[:] connectivity = cpm.mesh.connectivity.data
    cdef double[:] connectivity_sums = cpm.mesh.connectivity_sums

    cdef int[:] cp_indices = cpm.mesh.copy_regions.indices
    cdef int[:] cp_indptr = cpm.mesh.copy_regions.indptr

    cdef double[:] area = cpm.mesh.area

    cdef int[:] celltype = cpm.celltype

    cdef double va

    cdef int bdd = 0 # Keep track of copy attempts for which source and target have different labels
    cdef int acc = 0 # Keep track of accepted copy attempts
    
    cdef int acc_up = 0 # Keep track of accepted uphill copy attempts

    cdef int i, j, ii, jj, kk
    cdef int ni, n2i, sigma, nsigma, n2sigma
    cdef int ct, nct, n2ct
    cdef double dH

    cdef double total_dH = 0.0
    
    cdef double s, t, n2p

#    cdef int chance0 = cpm.chance0
#    cdef int chance1 = cpm.chance1

    cdef double lambda_area = cpm.lambda_area
    cdef double lambda_perimeter = cpm.lambda_perimeter #/cpm.xi/cpm.xi

    cdef double dperimeter, dnperimeter, darea, dnarea
    
#    cdef double[:] copyprob = cpm.copyprob
    cdef double temperature = cpm.temperature

    cdef double total_area = cpm.total_area
    cdef double[:] cum_area = cpm.cumulative_area

    for _ in range(n_steps):
# Randomly select target vertex
        v = ldexp(pcg32_random(), -32)*total_area # (rounded down, so less than total area)
        # Do binary search to find i
        i = bsearch(v, cum_area, 0, Nv-1)
        
#        print v, i, cum_area[i], cum_area[i-1] if i>0 else 0
        #i = pcg32_boundedrand(Nv)    
# If no neighbours return to selecting a target vertex
        if indptr[i]==indptr[i+1]:
            continue
# Weight selection of neighbour as source vertex depending on connectivity weights
        s = connectivity_sums[i]*ldexp(pcg32_random(), -32)
        t = 0.0
        for jj in range(indptr[i], indptr[i+1]):
            t+=connectivity[jj]
            if t>=s:
                break

        ni = indices[jj]

        sigma = state[i]
        nsigma = state[ni]
        if sigma != nsigma: # Quick exit if copy does nothing
            ct = celltype[sigma]
            nct = celltype[nsigma]
            
            dperimeter = 0.0
            dnperimeter = 0.0
            darea = 0.0
            dnarea = 0.0
            bdd += 1 
            dH = 0


            # Need to loop over the target site i and its neighbours separately.

            va = area[i]

            darea += -va
            dnarea += va

            for j in range(ep_indptr[i], ep_indptr[i+1]):
                n2i = ep_indices[j]
                n2p = ep[j]
                n2sigma = state[n2i]
                n2ct = celltype[n2sigma]

                # Need to check if  n2 is within copy region
                if n2sigma == sigma:
                    flg = 0
                    for kk in range(cp_indptr[i], cp_indptr[i+1]):
                        if n2i == cp_indices[kk]:
                            flg = 1
                            break
                    if flg == 1:
                        continue

                    
                if sigma != n2sigma:
                    dperimeter += -n2p
                    dH -= J[ct, n2ct]*n2p
                else:
                    dperimeter += n2p

                if nsigma != n2sigma:
                    dnperimeter += n2p
                    dH += J[nct, n2ct]*n2p
                else:
                    dnperimeter -= n2p


            # Neighbours of i in the copy region who have same label sigma (otherwise have to consider many cell area changes)
            for jj in range(cp_indptr[i], cp_indptr[i+1]):
                ii = cp_indices[jj]
                if state[ii] != sigma:
                    continue

                va = area[ii]
                darea += -va
                dnarea += va

                for j in range(ep_indptr[ii], ep_indptr[ii+1]):
                    n2i = ep_indices[j]
                    n2p = ep[j]
                    n2sigma = state[n2i]
                    n2ct = celltype[n2sigma]


                    # Check to see if this site is actually in the copy region - skip if so
                    if n2sigma==sigma:
                        flg = 0
                        if n2i == i:
                            flg = 1
                        else:
                            for kk in range(cp_indptr[i], cp_indptr[i+1]):
                                if n2i == cp_indices[kk]:
                                    flg = 1
                                    break
                        if flg == 1:
                            continue
                                
                    if sigma != n2sigma:
                        dperimeter += -n2p
                        dH -= J[ct, n2ct]*n2p
                    else:
                        dperimeter += n2p

                    if nsigma != n2sigma:
                        dnperimeter += n2p
                        dH += J[nct, n2ct]*n2p
                    else:
                        dnperimeter -= n2p


            if sigma>0:
                dH += lambda_perimeter * (dperimeter*dperimeter + 2*dperimeter * (_perimeter[sigma] - target_perimeter[sigma]))
                dH += lambda_area * (darea*darea + 2*darea*(_area[sigma] - target_area[sigma]))
            if nsigma>0:
                dH += lambda_perimeter * (dnperimeter*dnperimeter + 2*dnperimeter * (_perimeter[nsigma] - target_perimeter[nsigma]))
                dH += lambda_area * (dnarea*dnarea + 2*dnarea*(_area[nsigma] - target_area[nsigma]))
                    

#            print '***'
#            print i, ni, sigma, nsigma, va, dperimeter, dnperimeter, dH
#            print _area[sigma], _area[nsigma], _perimeter[sigma], _perimeter[nsigma]
#            print target_area[sigma], target_area[nsigma], target_perimeter[sigma], target_perimeter[nsigma]
            

            # Decide whether to accept copy attempt
            if dH < 0 or ldexp(pcg32_random(), -32) < exp(-dH/temperature):               

                # perform copy
#                if sigma>0:


                _area[sigma] += darea
                _perimeter[sigma] += dperimeter
                    
#                if nsigma>0:
                _area[nsigma] += dnarea
                _perimeter[nsigma] += dnperimeter

                state[i] = state[ni]

                for jj in range(cp_indptr[i], cp_indptr[i+1]):
                    ii = cp_indices[jj]
                    if state[ii] == sigma:
                        state[ii] = state[ni]


                acc += 1
                if dH>0:
                    acc_up += 1

                total_dH += dH
#    print n_steps, bdd, acc
    return acc, bdd, acc_up, total_dH


@cython.boundscheck(False)
@cython.wraparound(False)
@cython.nonecheck(False)
@cython.cdivision(True)
def evolve_CPM_bdd(cpm, int n_steps):

    cdef int Nv = len(cpm.state)
    cdef int[:] state = cpm.state
    cdef int[:] bdd_state = cpm.bdd_state
    cdef double[:] _area = cpm._area
    cdef double[:] _perimeter = cpm._perimeters

    cdef double[:,:] J = cpm.J

    cdef double[:] target_perimeter = cpm.target_perimeter
    cdef double[:] target_area = cpm.target_area


    cdef int[:] ep_indices = cpm.mesh.perimeters.indices
    cdef int[:] ep_indptr = cpm.mesh.perimeters.indptr
    cdef double[:] ep = cpm.mesh.perimeters.data
    cdef double[:] ep_sums = cpm.mesh.perimeter_sums

    cdef int[:] indices = cpm.mesh.connectivity.indices
    cdef int[:] indptr = cpm.mesh.connectivity.indptr
    cdef double[:] connectivity = cpm.mesh.connectivity.data
    cdef double[:] connectivity_sums = cpm.mesh.connectivity_sums

    cdef int[:] cp_indices = cpm.mesh.copy_regions.indices
    cdef int[:] cp_indptr = cpm.mesh.copy_regions.indptr

    cdef double[:] area = cpm.mesh.area

    cdef int[:] celltype = cpm.celltype

    cdef double va

    cdef int bdd = 0 # Keep track of copy attempts for which source and target have different labels
    cdef int acc = 0 # Keep track of accepted copy attempts
    
    cdef int acc_up = 0 # Keep track of accepted uphill copy attempts

    cdef int i, j, ii, jj, kk, iii, jjj
    cdef int ni, n2i, sigma, nsigma, n2sigma
    cdef int ct, nct, n2ct
    cdef double dH

    cdef double total_dH = 0.0
    
    cdef double s, t, n2p

#    cdef int chance0 = cpm.chance0
#    cdef int chance1 = cpm.chance1

    cdef double lambda_area = cpm.lambda_area
    cdef double lambda_perimeter = cpm.lambda_perimeter #/cpm.xi/cpm.xi

    cdef double dperimeter, dnperimeter, darea, dnarea
    
#    cdef double[:] copyprob = cpm.copyprob
    cdef double temperature = cpm.temperature

    cdef double total_area = cpm.total_area
    cdef double[:] cum_area = cpm.cumulative_area

    for _ in range(n_steps):
# Randomly select target vertex
        v = ldexp(pcg32_random(), -32)*total_area # (rounded down, so less than total area)
        # Do binary search to find i
        i = bsearch(v, cum_area, 0, Nv-1)

        if not bdd_state[i]:
            continue
#        print v, i, cum_area[i], cum_area[i-1] if i>0 else 0
        #i = pcg32_boundedrand(Nv)    
# If no neighbours return to selecting a target vertex
        if indptr[i]==indptr[i+1]:
            continue
# Weight selection of neighbour as source vertex depending on connectivity weights
        s = connectivity_sums[i]*ldexp(pcg32_random(), -32)
        t = 0.0
        for jj in range(indptr[i], indptr[i+1]):
            t+=connectivity[jj]
            if t>=s:
                break

        ni = indices[jj]
        
        if not bdd_state[ni]:
            continue
        
        sigma = state[i]
        nsigma = state[ni]
        if sigma != nsigma: # Quick exit if copy does nothing
            ct = celltype[sigma]
            nct = celltype[nsigma]
            
            dperimeter = 0.0
            dnperimeter = 0.0
            darea = 0.0
            dnarea = 0.0
            bdd += 1 
            dH = 0


            # Need to loop over the target site i and its neighbours separately.

            va = area[i]

            darea += -va
            dnarea += va

            for j in range(ep_indptr[i], ep_indptr[i+1]):
                n2i = ep_indices[j]
                n2p = ep[j]
                n2sigma = state[n2i]
                n2ct = celltype[n2sigma]

                # Need to check if  n2 is within copy region
                if n2sigma == sigma:
                    flg = 0
                    for kk in range(cp_indptr[i], cp_indptr[i+1]):
                        if n2i == cp_indices[kk]:
                            flg = 1
                            break
                    if flg == 1:
                        continue

                    
                if sigma != n2sigma:
                    dperimeter += -n2p
                    dH -= J[ct, n2ct]*n2p
                else:
                    dperimeter += n2p

                if nsigma != n2sigma:
                    dnperimeter += n2p
                    dH += J[nct, n2ct]*n2p
                else:
                    dnperimeter -= n2p


            # Neighbours of i in the copy region who have same label sigma (otherwise have to consider many cell area changes)
            for jj in range(cp_indptr[i], cp_indptr[i+1]):
                ii = cp_indices[jj]
                if state[ii] != sigma:
                    continue

                va = area[ii]
                darea += -va
                dnarea += va

                for j in range(ep_indptr[ii], ep_indptr[ii+1]):
                    n2i = ep_indices[j]
                    n2p = ep[j]
                    n2sigma = state[n2i]
                    n2ct = celltype[n2sigma]


                    # Check to see if this site is actually in the copy region - skip if so
                    if n2sigma==sigma:
                        flg = 0
                        if n2i == i:
                            flg = 1
                        else:
                            for kk in range(cp_indptr[i], cp_indptr[i+1]):
                                if n2i == cp_indices[kk]:
                                    flg = 1
                                    break
                        if flg == 1:
                            continue
                                
                    if sigma != n2sigma:
                        dperimeter += -n2p
                        dH -= J[ct, n2ct]*n2p
                    else:
                        dperimeter += n2p

                    if nsigma != n2sigma:
                        dnperimeter += n2p
                        dH += J[nct, n2ct]*n2p
                    else:
                        dnperimeter -= n2p


            if sigma>0:
                dH += lambda_perimeter * (dperimeter*dperimeter + 2*dperimeter * (_perimeter[sigma] - target_perimeter[sigma]))
                dH += lambda_area * (darea*darea + 2*darea*(_area[sigma] - target_area[sigma]))
            if nsigma>0:
                dH += lambda_perimeter * (dnperimeter*dnperimeter + 2*dnperimeter * (_perimeter[nsigma] - target_perimeter[nsigma]))
                dH += lambda_area * (dnarea*dnarea + 2*dnarea*(_area[nsigma] - target_area[nsigma]))
                    

#            print '***'
#            print i, ni, sigma, nsigma, va, dperimeter, dnperimeter, dH
#            print _area[sigma], _area[nsigma], _perimeter[sigma], _perimeter[nsigma]
#            print target_area[sigma], target_area[nsigma], target_perimeter[sigma], target_perimeter[nsigma]
            

            # Decide whether to accept copy attempt
            if dH < 0 or ldexp(pcg32_random(), -32) < exp(-dH/temperature):               

                # perform copy
#                if sigma>0:


                _area[sigma] += darea
                _perimeter[sigma] += dperimeter
                    
#                if nsigma>0:
                _area[nsigma] += dnarea
                _perimeter[nsigma] += dnperimeter

                state[i] = state[ni]

                for jj in range(cp_indptr[i], cp_indptr[i+1]):
                    ii = cp_indices[jj]
                    if state[ii] == sigma:
                        state[ii] = state[ni]
                        for jjj in range(indptr[ii], indptr[ii+1]):
                            iii = indices[jjj]
                            if state[iii] == nsigma:
                                bdd_state[ii]-=1
                                bdd_state[iii]-=1
                            elif state[iii] == sigma:
                                bdd_state[ii]+=1
                                bdd_state[iii]+=1
                                
                ## fix up bdd_state?

                
                acc += 1
                if dH>0:
                    acc_up += 1

                total_dH += dH
#    print n_steps, bdd, acc
    return acc, bdd, acc_up, total_dH
