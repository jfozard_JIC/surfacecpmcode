

import numpy as np
cimport numpy as np
import cython
cimport cython
cimport cpython

from libc.stdlib cimport rand, RAND_MAX
from libc.math cimport sqrt, ldexp, exp

from random import randint, random

cdef double EPSILON = 1e-16

cdef extern unsigned int pcg32_random()
cdef extern void pcg32_srandom(unsigned long, unsigned long)
cdef extern unsigned int pcg32_boundedrand(unsigned int)

pcg32_srandom(32, 48)

@cython.boundscheck(False)
@cython.wraparound(False)
@cython.nonecheck(False)
@cython.cdivision(True)
def evolve_CPM(cpm, int n_steps):

    cdef int Nv = len(cpm.state)
    cdef int[:] state = cpm.state
    cdef double[:] _area = cpm._area
    cdef double[:] _perimeter = cpm._perimeters

    cdef double[:,:] J = cpm.J

    cdef double[:] target_perimeter = cpm.target_perimeter
    cdef double[:] target_area = cpm.target_area


    cdef int[:] ep_indices = cpm.mesh.perimeters.indices
    cdef int[:] ep_indptr = cpm.mesh.perimeters.indptr
    cdef double[:] ep = cpm.mesh.perimeters.data
    cdef double[:] ep_sums = cpm.mesh.perimeter_sums

    cdef int[:] indices = cpm.mesh.connectivity.indices
    cdef int[:] indptr = cpm.mesh.connectivity.indptr
    cdef double[:] connectivity = cpm.mesh.connectivity.data
    cdef double[:] connectivity_sums = cpm.mesh.connectivity_sums

    cdef double[:] area = cpm.mesh.area

    cdef int[:] celltype = cpm.celltype

    cdef double va

    cdef int bdd = 0 # Keep track of copy attempts for which source and target have different labels
    cdef int acc = 0 # Keep track of accepted copy attempts
    
    cdef int acc_up = 0 # Keep track of accepted uphill copy attempts

    cdef int i, j, jj, k
    cdef int ni, n2i, sigma, nsigma, n2sigma
    cdef int ct, nct, n2ct
    cdef double dH

    cdef double total_dH = 0.0
    
    cdef double s, t, n2p

#    cdef int chance0 = cpm.chance0
#    cdef int chance1 = cpm.chance1

    cdef double lambda_area = cpm.lambda_area
    cdef double lambda_perimeter = cpm.lambda_perimeter #/cpm.xi/cpm.xi

#    cdef double[:] copyprob = cpm.copyprob
    cdef double temperature = cpm.temperature


    for _ in range(n_steps):
# Randomly select target vertex
        i = pcg32_boundedrand(Nv)    
# If no neighbours return to selecting a target vertex
        if indptr[i]==indptr[i+1]:
            continue
# Weight selection of neighbour as source vertex depending on connectivity weights
        s = connectivity_sums[i]*ldexp(pcg32_random(), -32)
        t = 0.0
        for jj in range(indptr[i], indptr[i+1]):
            t+=connectivity[jj]
            if t>=s:
                break

        ni = indices[jj]

        sigma = state[i]
        nsigma = state[ni]
        if sigma != nsigma: # Quick exit if copy does nothing
            ct = celltype[sigma]
            nct = celltype[nsigma]
            
            dperimeter = 0.0
            dnperimeter = 0.0
            va = area[i]
            bdd += 1 
            dH = 0

            for j in range(ep_indptr[i], ep_indptr[i+1]):
                n2i = ep_indices[j]
                n2p = ep[j]
                n2sigma = state[n2i]
                n2ct = celltype[n2sigma]

                if sigma != n2sigma:
                    dperimeter += -n2p
                    dH -= J[ct, n2ct]*n2p
                else:
                    dperimeter += n2p

                if nsigma != n2sigma:
                    dnperimeter += n2p
                    dH += J[nct, n2ct]*n2p
                else:
                    dnperimeter -= n2p


            if sigma>0:
                dH += lambda_perimeter * (dperimeter*dperimeter + 2*dperimeter * (_perimeter[sigma] - target_perimeter[sigma]))
                dH += lambda_area * (va*va - 2*va*(_area[sigma] - target_area[sigma]))
            if nsigma>0:
                dH += lambda_perimeter * (dnperimeter*dnperimeter + 2*dnperimeter * (_perimeter[nsigma] - target_perimeter[nsigma]))
                dH += lambda_area * (va*va + 2*va*(_area[nsigma] - target_area[nsigma]))
                    

#            print '***'
#            print i, ni, sigma, nsigma, va, dperimeter, dnperimeter, dH
#            print _area[sigma], _area[nsigma], _perimeter[sigma], _perimeter[nsigma]
#            print target_area[sigma], target_area[nsigma], target_perimeter[sigma], target_perimeter[nsigma]
            

            # Decide whether to accept copy attempt
#            if dH < chance0 and (dH < chance1 or
#                                 ldexp(pcg32_random(), -32) < copyprob[<int>(dH) - chance1 - 1]):               
            if dH < 0 or ldexp(pcg32_random(), -32) < exp(-dH/temperature):

                # perform copy
#                if sigma>0:


                _area[sigma] -= va
                _perimeter[sigma] += dperimeter
                    
#                if nsigma>0:
                _area[nsigma] += va
                _perimeter[nsigma] += dnperimeter

                state[i] = state[ni]
                acc += 1
                if dH>0:
                    acc_up += 1
                total_dH += dH
#    print n_steps, bdd, acc
    return acc, bdd, acc_up, total_dH

@cython.boundscheck(False)
@cython.wraparound(False)
@cython.nonecheck(False)
@cython.cdivision(True)
def evolve_CPM_bdd(cpm, int n_steps):

    cdef int Nv = len(cpm.state)
    cdef int[:] state = cpm.state
    cdef int[:] bdd_state = cpm.bdd_state
    cdef double[:] _area = cpm._area
    cdef double[:] _perimeter = cpm._perimeters

    cdef double[:,:] J = cpm.J

    cdef double[:] target_perimeter = cpm.target_perimeter
    cdef double[:] target_area = cpm.target_area


    cdef int[:] ep_indices = cpm.mesh.perimeters.indices
    cdef int[:] ep_indptr = cpm.mesh.perimeters.indptr
    cdef double[:] ep = cpm.mesh.perimeters.data
    cdef double[:] ep_sums = cpm.mesh.perimeter_sums

    cdef int[:] indices = cpm.mesh.connectivity.indices
    cdef int[:] indptr = cpm.mesh.connectivity.indptr
    cdef double[:] connectivity = cpm.mesh.connectivity.data
    cdef double[:] connectivity_sums = cpm.mesh.connectivity_sums

    cdef double[:] area = cpm.mesh.area

    cdef int[:] celltype = cpm.celltype

    cdef double va

    cdef int bdd = 0 # Keep track of copy attempts for which source and target have different labels
    cdef int acc = 0 # Keep track of accepted copy attempts
    
    cdef int acc_up = 0 # Keep track of accepted uphill copy attempts

    cdef int i, j, jj, k
    cdef int ni, n2i, sigma, nsigma, n2sigma
    cdef int ct, nct, n2ct
    cdef double dH

    cdef int bdd_new, bdd_new2, n3i
    
    cdef double total_dH = 0.0
    
    cdef double s, t, n2p

#    cdef int chance0 = cpm.chance0
#    cdef int chance1 = cpm.chance1

    cdef double lambda_area = cpm.lambda_area
    cdef double lambda_perimeter = cpm.lambda_perimeter #/cpm.xi/cpm.xi

#    cdef double[:] copyprob = cpm.copyprob
    cdef double temperature = cpm.temperature


    for _ in range(n_steps):
# Randomly select target vertex
        i = pcg32_boundedrand(Nv)    
# If no neighbours return to selecting a target vertex
        if not bdd_state[i]:
            continue
        if indptr[i]==indptr[i+1]:
            continue
# Weight selection of neighbour as source vertex depending on connectivity weights
        s = connectivity_sums[i]*ldexp(pcg32_random(), -32)
        t = 0.0
        for jj in range(indptr[i], indptr[i+1]):
            t+=connectivity[jj]
            if t>=s:
                break

        ni = indices[jj]
        if not bdd_state[ni]:
            continue
        
        sigma = state[i]
        nsigma = state[ni]
        if sigma != nsigma: # Quick exit if copy does nothing
            ct = celltype[sigma]
            nct = celltype[nsigma]
            
            dperimeter = 0.0
            dnperimeter = 0.0
            va = area[i]
            bdd += 1 
            dH = 0

            for j in range(ep_indptr[i], ep_indptr[i+1]):
                n2i = ep_indices[j]
                n2p = ep[j]
                n2sigma = state[n2i]
                n2ct = celltype[n2sigma]

                if sigma != n2sigma:
                    dperimeter += -n2p
                    dH -= J[ct, n2ct]*n2p
                else:
                    dperimeter += n2p

                if nsigma != n2sigma:
                    dnperimeter += n2p
                    dH += J[nct, n2ct]*n2p
                else:
                    dnperimeter -= n2p


            if sigma>0:
                dH += lambda_perimeter * (dperimeter*dperimeter + 2*dperimeter * (_perimeter[sigma] - target_perimeter[sigma]))
                dH += lambda_area * (va*va - 2*va*(_area[sigma] - target_area[sigma]))
            if nsigma>0:
                dH += lambda_perimeter * (dnperimeter*dnperimeter + 2*dnperimeter * (_perimeter[nsigma] - target_perimeter[nsigma]))
                dH += lambda_area * (va*va + 2*va*(_area[nsigma] - target_area[nsigma]))
                    

#            print '***'
#            print i, ni, sigma, nsigma, va, dperimeter, dnperimeter, dH
#            print _area[sigma], _area[nsigma], _perimeter[sigma], _perimeter[nsigma]
#            print target_area[sigma], target_area[nsigma], target_perimeter[sigma], target_perimeter[nsigma]
            

            # Decide whether to accept copy attempt
#            if dH < chance0 and (dH < chance1 or
#                                 ldexp(pcg32_random(), -32) < copyprob[<int>(dH) - chance1 - 1]):               
            if dH < 0 or ldexp(pcg32_random(), -32) < exp(-dH/temperature):

                # perform copy
#                if sigma>0:


                _area[sigma] -= va
                _perimeter[sigma] += dperimeter
                    
#                if nsigma>0:
                _area[nsigma] += va
                _perimeter[nsigma] += dnperimeter

                state[i] = state[ni]
# Fix up bdd state
                bdd_new = 0
                for j in range(indptr[i], indptr[i+1]):
                    n2i = indices[j]
                    n2sigma = state[n2i]
                    if n2sigma==nsigma:
                        bdd_new2 = 0
                        for k in range(indptr[n2i], indptr[n2i+1]):
                            n3i = indices[k]
                            if n2sigma != state[n3i]:
                                bdd_new2 = 1
                                break
                        bdd_state[n2i] = bdd_new2
                    else:
                        bdd_state[n2i] = 1
                        bdd_new = 1
                bdd_state[i] = bdd_new
                acc += 1
                if dH>0:
                    acc_up += 1
                total_dH += dH
#    print n_steps, bdd, acc
    return acc, bdd, acc_up, total_dH


@cython.boundscheck(False)
@cython.wraparound(False)
@cython.nonecheck(False)
@cython.cdivision(True)
def evolve_CPM_bdd2(cpm, int n_steps):

    cdef int Nv = len(cpm.state)
    cdef int[:] state = cpm.state
    cdef int[:] bdd_state = cpm.bdd_state
    cdef double[:] _area = cpm._area
    cdef double[:] _perimeter = cpm._perimeters

    cdef double[:,:] J = cpm.J

    cdef double[:] target_perimeter = cpm.target_perimeter
    cdef double[:] target_area = cpm.target_area


    cdef int[:] ep_indices = cpm.mesh.perimeters.indices
    cdef int[:] ep_indptr = cpm.mesh.perimeters.indptr
    cdef double[:] ep = cpm.mesh.perimeters.data
    cdef double[:] ep_sums = cpm.mesh.perimeter_sums

    cdef int[:] indices = cpm.mesh.connectivity.indices
    cdef int[:] indptr = cpm.mesh.connectivity.indptr
    cdef double[:] connectivity = cpm.mesh.connectivity.data
    cdef double[:] connectivity_sums = cpm.mesh.connectivity_sums

    cdef double[:] area = cpm.mesh.area

    cdef int[:] celltype = cpm.celltype

    cdef double va

    cdef int bdd = 0 # Keep track of copy attempts for which source and target have different labels
    cdef int acc = 0 # Keep track of accepted copy attempts
    
    cdef int acc_up = 0 # Keep track of accepted uphill copy attempts

    cdef int i, j, jj, k
    cdef int ni, n2i, sigma, nsigma, n2sigma
    cdef int ct, nct, n2ct
    cdef double dH

    cdef int bdd_new, bdd_new2, n3i
    
    cdef double total_dH = 0.0
    
    cdef double s, t, n2p

#    cdef int chance0 = cpm.chance0
#    cdef int chance1 = cpm.chance1

    cdef double lambda_area = cpm.lambda_area
    cdef double lambda_perimeter = cpm.lambda_perimeter #/cpm.xi/cpm.xi

#    cdef double[:] copyprob = cpm.copyprob
    cdef double temperature = cpm.temperature


    for _ in range(n_steps):
# Randomly select target vertex
        i = pcg32_boundedrand(Nv)    
# If no neighbours return to selecting a target vertex
        if not bdd_state[i]:
            continue
        if indptr[i]==indptr[i+1]:
            continue
# Weight selection of neighbour as source vertex depending on connectivity weights
        s = connectivity_sums[i]*ldexp(pcg32_random(), -32)
        t = 0.0
        for jj in range(indptr[i], indptr[i+1]):
            t+=connectivity[jj]
            if t>=s:
                break

        ni = indices[jj]
        if not bdd_state[ni]:
            continue
        
        sigma = state[i]
        nsigma = state[ni]
        if sigma != nsigma: # Quick exit if copy does nothing
            ct = celltype[sigma]
            nct = celltype[nsigma]
            
            dperimeter = 0.0
            dnperimeter = 0.0
            va = area[i]
            bdd += 1 
            dH = 0

            for j in range(ep_indptr[i], ep_indptr[i+1]):
                n2i = ep_indices[j]
                n2p = ep[j]
                n2sigma = state[n2i]
                n2ct = celltype[n2sigma]

                if sigma != n2sigma:
                    dperimeter += -n2p
                    dH -= J[ct, n2ct]*n2p
                else:
                    dperimeter += n2p

                if nsigma != n2sigma:
                    dnperimeter += n2p
                    dH += J[nct, n2ct]*n2p
                else:
                    dnperimeter -= n2p


            if sigma>0:
                dH += lambda_perimeter * (dperimeter*dperimeter + 2*dperimeter * (_perimeter[sigma] - target_perimeter[sigma]))
                dH += lambda_area * (va*va - 2*va*(_area[sigma] - target_area[sigma]))
            if nsigma>0:
                dH += lambda_perimeter * (dnperimeter*dnperimeter + 2*dnperimeter * (_perimeter[nsigma] - target_perimeter[nsigma]))
                dH += lambda_area * (va*va + 2*va*(_area[nsigma] - target_area[nsigma]))
                    

#            print '***'
#            print i, ni, sigma, nsigma, va, dperimeter, dnperimeter, dH
#            print _area[sigma], _area[nsigma], _perimeter[sigma], _perimeter[nsigma]
#            print target_area[sigma], target_area[nsigma], target_perimeter[sigma], target_perimeter[nsigma]
            

            # Decide whether to accept copy attempt
#            if dH < chance0 and (dH < chance1 or
#                                 ldexp(pcg32_random(), -32) < copyprob[<int>(dH) - chance1 - 1]):               
            if dH < 0 or ldexp(pcg32_random(), -32) < exp(-dH/temperature):

                # perform copy
#                if sigma>0:


                _area[sigma] -= va
                _perimeter[sigma] += dperimeter
                    
#                if nsigma>0:
                _area[nsigma] += va
                _perimeter[nsigma] += dnperimeter

                state[i] = state[ni]
# Fix up bdd state
#                bdd_new = 0
                for j in range(indptr[i], indptr[i+1]):
                    n2i = indices[j]
                    n2sigma = state[n2i]
                    if n2sigma==nsigma:
                        bdd_state[n2i]-=1
                        bdd_state[i]-=1
                    elif n2sigma==sigma:
                        bdd_state[n2i]+=1
                        bdd_state[i]+=1
                acc += 1
                if dH>0:
                    acc_up += 1
                total_dH += dH
#    print n_steps, bdd, acc
    return acc, bdd, acc_up, total_dH
