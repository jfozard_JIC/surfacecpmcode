
import numpy as np
import scipy.ndimage as nd
import matplotlib.pyplot as plt

import scipy.ndimage as nd
from math import sqrt, ceil, floor, log, exp, atan2

from cpm_step import evolve_CPM
from scipy.misc import imsave

import numpy.random as npr
import sys

from time import sleep
import numpy.linalg as la

from meshSPM_weighted2 import MeshSPM
from scipy.sparse import dok_matrix, csr_matrix


lut = npr.randint(256, size=(20000, 3)).astype(np.uint8)

lut[0,:] = lut[0,:]/10

lut_float = lut.astype(float)/256.0


def col_png(im):
    img = lut_float[im,:]
    return img

def angle(u):
    eigs, evec = la.eig([[u[0], u[1]], [u[1], u[2]]])
    v = evec[:,np.argmax(eigs)]
    if v[0]>0:
        return atan2(v[1], v[0])
    else:
        return atan2(-v[1], -v[0])



class TriangleMesh(object):
    def __init__(self, verts, tris):
        
        self.verts = np.asarray(verts, dtype=float)
        self.tris = np.asarray(tris, dtype=np.int32)
        self.Nv = len(tris)

        self.make_adjacency_matrix()

    def make_tri_neighbours(self):

        self.tri_neighbours = np.zeros(self.tris.shape, dtype=int)
        N = self.verts.shape[0]

        tris = np.asarray(self.tris)

        edges = (tris[:,[1,2,0]] + N*tris[:, [2,0,1]]).flatten()
        rev_edges = (tris[:, [2,0,1]] + N*tris[:, [1,2,0]]).flatten()
        
        sort_idx = np.argsort(edges)
        sorted_edges = edges[sort_idx]
        
        idx = np.searchsorted(sorted_edges, rev_edges)
        idx[idx==sort_idx.shape[0]] = 0

        m = sorted_edges[idx]!=rev_edges

        idx = sort_idx[idx]
        idx[m] = -1

        self.tri_neighbours = idx.reshape(-1,3)

    def make_adjacency_matrix(self):
        # Specialized for triangles

        tri_edges = la.norm(self.verts[self.tris[:,[1,2,0]],:]-self.verts[self.tris[:,[2,0,1]],:], axis=2)

        i = np.repeat(np.arange(n),3)
        j = self.tri_neighbours/3
        j = j.flatten()
        data = tri_edges.flatten()
        idx = np.where(j>=0)[0]
        i = i[idx]
        j = j[idx]
        data = data[idx]
        print i.shape, j.shape, data.shape

        A = csr_matrix((data.flatten(), (i,j.flatten())), (n,n), dtype=np.float32)
        self.adjacency = A
        self.adjacency_sums = np.flatten(A.sum(axis=1))

    def make_perimeter_matrix(self, r):
        # This is the most difficult bit, and needs to be reasonably efficient
        pass
        



def is_array(o):
    return isinstance(o, (list, tuple, np.ndarray))

class RegularTriangleMesh(TriangleMesh):
    def __init__(self, m, n, dm, dn):
        self.m = m
        self.n = n

        dm = np.asarray(dm)
        dn = np.asarray(dn)
        self.verts = np.array([i*dm+j*dn for i in range(m) for j in range(n)])
        self.tris = chain([[n*i+j, n*i+j+1, n*(i+1)+j], [n*i+j+1, (n+1)*i+j+1, n*(i+1)+j]] for i in range(m) for j in range(n))

        self.verts = np.asarray(verts, dtype=float)
        self.tris = np.asarray(tris, dtype=np.int32)
        self.Nv = len(tris)

        self.make_adjacency_matrix()

        
lut = npr.randint(256, size=(10000, 3)).astype(np.uint8)
lut[0,:] *= 0.1

def show_tri_mesh_matplotlib(m):
    plt.figure()
    
    

plt.ion()
if __name__=="__main__":

    plt.figure()
    plt.imshow(im.T, cmap=plt.cm.gray)
    plt.draw()
    plt.pause(0.2)

    mesh = ImageMesh(im)

    data =[]
    angles = []

    for i in range(1):
        mSPM = MeshSPM(mesh, im.flatten(), 10)
        mSPM.nsteps = 100

        for i in range(500):
            mSPM.Run()
            
            show_tri_mesh(m)
            plt.draw()
            plt.pause(0.01)

