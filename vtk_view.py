

"""
Usage: python vtk_view.py infile.ply outfile.vtk
"""


import vtk
import numpy as np
from vtk.util.numpy_support import numpy_to_vtk, vtk_to_numpy, numpy_to_vtkIdTypeArray


import sys


def mkVtkIdList(it):
    vil = vtk.vtkIdList()
    for i in it:
        vil.InsertNextId(int(i))
    return vil


def load_gmsh(filename):
    """
    Use gmsh to generate a triangulated mesh of a polygon
    """
    
    f = open(filename,"r")
    

    l = f.readline
    while l:
        l = f.readline()
        if l == "$Nodes\n":
            break
    l = f.readline()
    nodes = int(l)
    verts = {}
    for i in range(nodes):
        l = f.readline().split()
        verts[int(l[0])]=np.array(map(float, l[1:4]))
    while l:
        l = f.readline()
        if l == "$Elements\n":
            break
    v_list = []
    v_map = {}
    for i,v in verts.iteritems():
        v_map[i] = len(v_list)
        v_list.append(v)

    elements = int(f.readline())
    vertex_pids = {}
    lines = []
    tris = []
    for i in range(elements):
        ll = f.readline().split()
        if ll[1]=="15": # Vertex
            vertex_pids[int(ll[4])] = int(ll[3])
        if ll[1]=="1": #line
            wid = int(ll[3])
            v0 = int(ll[-2])
            v1 = int(ll[-1])
#            lines.append([wid, v0, v1])
            lines.append((v_map[v0], v_map[v1]))
        if ll[1]=="2":
            t = tuple(map(int, ll[-3:]))
            i0, i1, i2 = t
            tris.append(tuple(map(v_map.get,t)))

    return v_list, lines, tris


v_list, lines, tris = load_gmsh(sys.argv[1])



vert_labels = []
verts = []
for v in v_list:
    verts.append(v)
    vert_labels.append(1)

vert_labels = np.array(vert_labels, dtype=float)

verts = np.array(verts, dtype=np.float64)

#verts = verts - np.mean(verts, axis=0)
#print verts
#verts = verts/np.max(np.abs(verts))


faces = []
face_labels = []

for i,f in enumerate(tris):
    faces.append(f)
    face_labels.append(i/1000)

face_labels = np.array(face_labels, dtype=float)
print 'face_labels', np.min(face_labels), np.max(face_labels)

ren = vtk.vtkRenderer()
renWin = vtk.vtkRenderWindow()
renWin.AddRenderer(ren)
 
# create a renderwindowinteractor
iren = vtk.vtkRenderWindowInteractor()
iren.SetRenderWindow(renWin)
 
# create points
points = vtk.vtkPoints()


points.SetData(numpy_to_vtk(verts))


cells = vtk.vtkCellArray() 

triangles = vtk.vtkCellArray()
for p in faces:
    triangles.InsertNextCell(mkVtkIdList(p))


trianglePolyData = vtk.vtkPolyData()
trianglePolyData.SetPoints( points )
#del points
trianglePolyData.SetPolys( triangles )
#trianglePolyData.GetPointData().SetScalars(numpy_to_vtk(vert_labels))
trianglePolyData.GetCellData().SetScalars(numpy_to_vtk(face_labels))

data = np.load(sys.argv[2])

class CallBack(object):
    def __init__(self):
        self.frame = 0
        self.N = data.shape[0]

    def update(self, obj, event):
        trianglePolyData.GetCellData().SetScalars(numpy_to_vtk(data[self.frame, :]))
        print self.frame, np.min(data[self.frame,:]), np.mean(data[self.frame,:]), np.max(data[self.frame,:])
        self.frame = (self.frame+1)%self.N
        obj.GetRenderWindow().Render()    
        
        
#writer = vtk.vtkPolyDataWriter()
#writer.SetFileName(sys.argv[2])
#writer.SetInputData(trianglePolyData)
#writer.Write()
 
# mapper
mapper = vtk.vtkPolyDataMapper()
if vtk.VTK_MAJOR_VERSION <= 5:
    mapper.SetInput(trianglePolyData)
else:
    mapper.SetInputData(trianglePolyData)

mapper.SetScalarRange(0, 40) 
# actor
actor = vtk.vtkActor()
actor.SetMapper(mapper)
 
# assign actor to the renderer
ren.AddActor(actor)

camera = vtk.vtkCamera()
#camera.SetPosition(100,100,100)
#camera.SetFocalPoint(0,0,0)
 



ren.SetActiveCamera(camera)
ren.ResetCamera()
ren.SetBackground(0,0,0)
 
renWin.SetSize(300,300)

# enable user interface interactor
iren.Initialize()

cb = CallBack()
iren.AddObserver('TimerEvent', cb.update)
timerId = iren.CreateRepeatingTimer(100);

renWin.Render()
iren.Start()
