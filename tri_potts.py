
""" 
Segmentation Potts model on a triangulated mesh,
which at some point could be changed to be a surface in 3D

Also think about solving simple PDEs on this mesh with 
linear basis elements

"""

import os, sys
import numpy as np
import matplotlib.pyplot as plt
from matplotlib.patches import CirclePolygon
from matplotlib.collections import PatchCollection, PolyCollection, LineCollection
from matplotlib.tri import Triangulation
import itertools
import numpy.random as npr
from PIL import Image
import scipy.ndimage as nd
from math import pi, sqrt, sin
from scipy.sparse import dok_matrix

import cairo

def mesh_polygon(poly, vol_max):
    """
    Use gmsh to generate a triangulated mesh of a polygon
    """
    
    f = open("test.geo","w")
    write_gmsh_poly(poly, f)
    os.system("gmsh test.geo -2 -clmax "+str(vol_max))
    f.close()
    f = open("test.msh","r")
    

    l = f.readline
    while l:
        l = f.readline()
        if l == "$Nodes\n":
            break
    l = f.readline()
    nodes = int(l)
    verts = {}
    for i in range(nodes):
        l = f.readline().split()
        verts[int(l[0])]=np.array(map(float, l[1:3]))
    while l:
        l = f.readline()
        if l == "$Elements\n":
            break
    v_list = []
    v_map = {}
    for i,v in verts.iteritems():
        v_map[i] = len(v_list)
        v_list.append(v)

    elements = int(f.readline())
    vertex_pids = {}
    lines = []
    tris = []
    for i in range(elements):
        ll = f.readline().split()
        if ll[1]=="15": # Vertex
            vertex_pids[int(ll[4])] = int(ll[3])
        if ll[1]=="1": #line
            wid = int(ll[3])
            v0 = int(ll[-2])
            v1 = int(ll[-1])
#            lines.append([wid, v0, v1])
            lines.append((v_map[v0], v_map[v1]))
        if ll[1]=="2":
            t = tuple(map(int, ll[-3:]))
            i0, i1, i2 = t
            tris.append(tuple(map(v_map.get,t)))

    return v_list, lines, tris


def write_gmsh_poly(poly, f):

    N = len(poly)
    for i, x in enumerate(poly):
        f.write("Point("+str(i+1)+")= { %f, %f, %f };"%(x[0], x[1], 0)+"\n")
        f.write("Physical Point("+str(i)+")= {"+str(i+1)+"};\n")    

    for i in range(N):
        f.write("Line ("+str(N+i+1)+") = {"+str(i+1)+", "+str((i+1)%N+1)+"};\n")
        f.write("Physical Line ("+str(N+i)+") = {"+str(N+i+1)+"};\n")
    
    f.write("Line Loop ("+str(2*N+1)+") = {")
    start = True
    for i in range(N):
        if not start:
            f.write(", ")
            f.write(str(N+i+1))
        else:
            f.write(str(N+i+1))
        start = False
    f.write("};\n")
    f.write("Plane Surface("+str(2*N+1)+") = {"+str(2*N+1)+"};\n")
    f.write("Physical Surface("+str(2*N+1)+") = {"+str(2*N+1)+"};\n")
    f.flush()
    f.close()


def calculate_dual(mesh):
    """
    Calculate the dual of the triangulation
    """
    pass
    

class Mesh(object):
    
    def __init__(self, verts, tris, bdd):
        self.verts = np.asarray(verts)
        self.tris = np.asarray(tris)
        self.tri_centroids = np.mean(self.verts[self.tris, :], axis=1)
        self.bdd = bdd

    @classmethod
    def from_polygon(cls, poly, h):
        """
        Method to construct mesh from polygonal region
        """
        
        v_list, lines, tris = mesh_polygon(poly, h*h)

        m = Mesh(v_list, tris, lines)
        return m

    def make_mpl_triangulation(self):
        v = np.asarray(self.verts)
        return Triangulation(v[:,0], v[:,1], self.tris)

    def draw_mesh(self):
        fig = plt.figure()
        pl = []
        for t in self.tris:
            poly = [self.verts[i] for i in t]
            pl.append(poly)
        pc = PolyCollection(pl, facecolor=(0.9, 0.9, 0.9))
        ll = []
        for v0, v1 in self.bdd:
            ll.append((self.verts[v0], self.verts[v1]))
        lc = LineCollection(ll, edgecolor=(1.0, 0.0, 0.0), linewidth=2.0)

        ax = fig.gca()
        ax.add_collection(pc, autolim=True)
        ax.add_collection(lc, autolim=True)
        
#        ax.autoscale_view()

        plt.show()

    def draw_mesh_signal(self):
        t = self.make_mpl_triangulation()
        plt.figure()
        plt.tripcolor(t, self.signal, shading='gouraud')
        plt.show()

    def draw_mesh_signal_points(self):
        # Calculate crude mesh bbox for point size
        bb = np.max(self.verts, axis=0)
        r0 = sqrt(bb[0]*bb[1]/len(self.verts)/pi)
        print self.verts[:2]
        plt.figure(1)
        fig = plt.figure()
        ax = plt.gca()
        polys = []
        for v in self.verts:
            polys.append(CirclePolygon(v, r0))
        cc = PatchCollection(polys, edgecolors='none')
        
        cc.set_array(self.signal.flatten())

        ax.add_collection(cc, autolim=True)
        ax.autoscale_view()
#        plt.show()


    def sample_image(self, image):
        print image.shape
        self.signal = nd.map_coordinates(image, np.asarray(self.verts).T)

    def calculate_tri_neighbours(self):

        self.tri_neighbours = np.zeros(self.tris.shape, dtype=int)

        N = self.verts.shape[0]

        tris = np.asarray(self.tris)

        edges = (tris[:,[1,2,0]] + N*tris[:, [2,0,1]]).flatten()
        rev_edges = (tris[:, [2,0,1]] + N*tris[:, [1,2,0]]).flatten()
        
        sort_idx = np.argsort(edges)
        sorted_edges = edges[sort_idx]
        
        idx = np.searchsorted(sorted_edges, rev_edges)
        idx[idx==sort_idx.shape[0]] = 0

        m = sorted_edges[idx]!=rev_edges

        idx = sort_idx[idx]
        idx[m] = -1

        self.tri_neighbours = idx.reshape(-1,3)


def draw_mesh_cairo(mesh, shape, tri_colours):
    WIDTH, HEIGHT = 800, 800

    surface = cairo.ImageSurface (cairo.FORMAT_ARGB32, WIDTH, HEIGHT)
    ctx = cairo.Context (surface)

    ctx.scale(WIDTH, HEIGHT) # Normalizing the canvas

    ctx.rectangle(0,0,1,1)
    ctx.set_source_rgb(1,1,1)
    ctx.fill()

    ctx.translate(0.05, 0.05)
    ctx.scale(0.9, 0.9)

    ctx.scale(1.0/shape[0], 1.0/shape[1])

    ctx.save()

    verts = mesh.verts
    centres = mesh.tri_centroids
    
    """
    ctx.save()
    # Draw tri dual boundaries
    ctx.set_source_rgb(0.7, 0.7, 0.7)
    ctx.set_line_width(0.5)

    for j, t in enumerate(mesh.tris):
        c = (verts[t[0]] + verts[t[1]] + verts[t[2]])/3.0

        ctx.move_to(*verts[t[i]])

        for i in range(3):
            ctx.line_to(*(0.5*(verts[t[(i+0)%3]] + verts[t[(i+1)%3]])))
            ctx.line_to(*c)
            ctx.line_to(*(0.5*(verts[t[(i+0)%3]] + verts[t[(i+2)%3]])))
            ctx.close_path()
            ctx.save()
        ctx.set_source_rgb(*vert_colours[t[i]])
        ctx.fill()
        ctx.restore()
        
        for i in range(3):
            ctx.move_to(*(0.5*(verts[t[(i+1)%3]] + verts[t[(i+2)%3]])))
            ctx.line_to(*c)
            ctx.save()
            if np.any(vert_colours[t[(i+1)%3]]!=vert_colours[t[(i+2)%3]]):
                ctx.set_line_width(0.8)
                ctx.set_source_rgb(0.2, 0.2, 0.2)
            else:
                ctx.set_source_rgb(0.7, 0.7, 0.7)
            ctx.stroke()
            ctx.restore()


    ctx.save()
    ctx.set_source_rgb(0.4, 0.4, 0.4)
    """

    ctx.save()


    ctx.save()
    ctx.set_line_width(0.4)

    # Draw cell triangles
    for j,t in enumerate(mesh.tris):
        ctx.move_to(*verts[t[0]])
        ctx.line_to(*verts[t[1]])
        ctx.line_to(*verts[t[2]])
        ctx.close_path()
        ctx.set_source_rgb(*tri_colours[j])
        ctx.fill_preserve()
        ctx.set_source_rgb(0.5, 0.5, 0.5)
        ctx.stroke()

    ctx.restore()


    ctx.save()
    # Draw tri dual boundaries
    ctx.set_source_rgb(0.7, 0.7, 0.7)
    ctx.set_line_width(0.5)

    for j, t in enumerate(mesh.tris):
        for i in range(3):
            nb = mesh.tri_neighbours[j, (i+2)%3]/3
            print j, nb
            if nb>=0:
                if np.any(tri_colours[nb]!=tri_colours[j]):
                    ctx.save()
                    ctx.set_line_width(0.8)
                    ctx.set_source_rgb(0.2, 0.2, 0.2)
                    ctx.move_to(*verts[t[i]])
                    ctx.line_to(*verts[t[(i+1)%3]])
                    ctx.stroke()
                    ctx.restore()
                """
                else:
                    ctx.save()
                    ctx.set_line_width(0.8)
                    ctx.set_source_rgb(0.5, 0.5, 0.5)
                    ctx.move_to(*verts[t[i]])
                    ctx.line_to(*verts[t[(i+1)%3]])
                    ctx.stroke()
                    ctx.restore()
                """

                ctx.save()
                ctx.set_source_rgb(0.7, 0.7, 0.7)
                ctx.move_to(*centres[j])
                ctx.line_to(*centres[nb])
                ctx.stroke()
                ctx.restore()
    ctx.restore()


    # Draw cell centres
    ctx.save()
    ctx.set_line_width(0.3)
    for v in mesh.tri_centroids:
        ctx.arc(v[0], v[1], 1.0, 0, 2*pi)
        ctx.set_source_rgb(0.7, 0.7, 0.7)
        ctx.fill_preserve()
        ctx.set_source_rgb(0.4, 0.4, 0.4)
        ctx.stroke()
    
    ctx.restore()
    
    ctx.restore()
    surface.write_to_png ("tri_potts.png") # Output to PNG

    

def main():
    Sx = 100
    Sy = 100
    m = Mesh.from_polygon([(0, 0), (Sx, 0), (Sx, Sy), (0, Sy)], 100.0)
    m.calculate_tri_neighbours()
    col_1 = [0.8, 0.5, 0.5]
    col_2 = [0.5, 0.8, 0.8]
    col_3 = [0.8, 0.6, 0.85]
    vc = [ col_1 if ((v[0]/float(Sx))**2 + (v[1]/float(Sy))**2)>0.7 else (col_2 if v[0]>v[1] else col_3) for v in m.tri_centroids]
    vc = np.array(vc)
    draw_mesh_cairo(m, (Sx, Sy), vc)
main()
