
import os, sys
import itertools
import numpy.random as npr
from PIL import Image
from math import pi, sqrt, sin
from scipy.sparse import coo_matrix

import numpy as np
import scipy.ndimage as nd
import matplotlib.pyplot as plt
from matplotlib.patches import CirclePolygon
from matplotlib.collections import PatchCollection, PolyCollection, LineCollection
from matplotlib.tri import Triangulation

from meshCPM import MeshCPM

import scipy.ndimage as nd
from math import sqrt, ceil, floor, log, exp, atan2

from scipy.misc import imsave

import numpy.random as npr
import sys

from time import sleep
import numpy.linalg as la

from scipy.sparse import dok_matrix, csr_matrix

from utils import pairs
from geom import area, centroid, ear_clip

from scipy.spatial import cKDTree
from scipy.spatial.distance import cdist

from meshCPM import MeshCPM

from calc_tri import *

from itertools import chain

# recursive implementation ....
def calc_poly_a3(pp1, pp2, r, level=0, max_level=2):
    # If level == max_level,
    if level == max_level:
#        print pp1, np.mean(pp1, axis=0)
        if la.norm(np.mean(pp1, axis=0) - np.mean(pp2, axis=0))<r:
            return area(pp1)*area(pp2)
        else:
            return 0.0
    # Fast return if near - furthest point pairs must lie on vertices
    dist_mtx = cdist(pp1, pp2)
    if np.max(dist_mtx)<r:
        return area(pp1)*area(pp2)
    # Split pp1 into 4; swap 1 and 2
    mid = [ 0.5*(pp1[1]+pp1[2]), 0.5*(pp1[0]+pp1[2]), 0.5*(pp1[0]+pp1[1]) ]
    """
           2
          / \
        m1  m0
        /     \
       0---m2--1
    
    So triangles are [0 m2 m1], [1 m0 m2], [2 m1 m0], [m0 m1 m2]
    """
    new_tris = [ [pp1[0], mid[2], mid[1]],
                 [pp1[1], mid[0], mid[2]],
                 [pp1[2], mid[1], mid[0]],
                 [mid[0], mid[1], mid[2]] ]
    return sum(calc_poly_a2(pp2, p, r, level+1, max_level) for p in new_tris)

# recursive implementation ....
def calc_poly_a4(pp1, pp2, r, level=0, max_level=2):
    # If level == max_level,
    if level == max_level:
#        print pp1, np.mean(pp1, axis=0)
        if la.norm(np.mean(pp1, axis=0) - np.mean(pp2, axis=0))<r:
            return 1.0
        else:
            return 0.0
    # Fast return if near - furthest point pairs must lie on vertices
    dist_mtx = cdist(pp1, pp2)
    if np.max(dist_mtx)<r:
        return 1.0
    # Split pp1 into 4; swap 1 and 2
    mid = [ 0.5*(pp1[1]+pp1[2]), 0.5*(pp1[0]+pp1[2]), 0.5*(pp1[0]+pp1[1]) ]
    """
           2
          / \
        m1  m0
        /     \
       0---m2--1
    
    So triangles are [0 m2 m1], [1 m0 m2], [2 m1 m0], [m0 m1 m2]
    """
    new_tris = [ [pp1[0], mid[2], mid[1]],
                 [pp1[1], mid[0], mid[2]],
                 [pp1[2], mid[1], mid[0]],
                 [mid[0], mid[1], mid[2]] ]
    return 0.25*sum(calc_poly_a4(pp2, p, r, level+1, max_level) for p in new_tris)


def mesh_polygon(poly, vol_max):
    """
    Use gmsh to generate a triangulated mesh of a polygon
    """
    
    f = open("test.geo","w")
    write_gmsh_poly(poly, f)
    os.system("gmsh test.geo -2 -clmax "+str(vol_max))
    f.close()
    f = open("test.msh","r")
    

    l = f.readline
    while l:
        l = f.readline()
        if l == "$Nodes\n":
            break
    l = f.readline()
    nodes = int(l)
    verts = {}
    for i in range(nodes):
        l = f.readline().split()
        verts[int(l[0])]=np.array(map(float, l[1:3]))
    while l:
        l = f.readline()
        if l == "$Elements\n":
            break
    v_list = []
    v_map = {}
    for i,v in verts.iteritems():
        v_map[i] = len(v_list)
        v_list.append(v)

    elements = int(f.readline())
    vertex_pids = {}
    lines = []
    tris = []
    for i in range(elements):
        ll = f.readline().split()
        if ll[1]=="15": # Vertex
            vertex_pids[int(ll[4])] = int(ll[3])
        if ll[1]=="1": #line
            wid = int(ll[3])
            v0 = int(ll[-2])
            v1 = int(ll[-1])
#            lines.append([wid, v0, v1])
            lines.append((v_map[v0], v_map[v1]))
        if ll[1]=="2":
            t = tuple(map(int, ll[-3:]))
            i0, i1, i2 = t
            tris.append(tuple(map(v_map.get,t)))

    return v_list, lines, tris


def write_gmsh_poly(poly, f):

    N = len(poly)
    for i, x in enumerate(poly):
        f.write("Point("+str(i+1)+")= { %f, %f, %f };"%(x[0], x[1], 0)+"\n")
        f.write("Physical Point("+str(i)+")= {"+str(i+1)+"};\n")    

    for i in range(N):
        f.write("Line ("+str(N+i+1)+") = {"+str(i+1)+", "+str((i+1)%N+1)+"};\n")
        f.write("Physical Line ("+str(N+i)+") = {"+str(N+i+1)+"};\n")
    
    f.write("Line Loop ("+str(2*N+1)+") = {")
    start = True
    for i in range(N):
        if not start:
            f.write(", ")
            f.write(str(N+i+1))
        else:
            f.write(str(N+i+1))
        start = False
    f.write("};\n")
    f.write("Plane Surface("+str(2*N+1)+") = {"+str(2*N+1)+"};\n")
    f.write("Physical Surface("+str(2*N+1)+") = {"+str(2*N+1)+"};\n")
    f.flush()
    f.close()




lut = npr.randint(256, size=(20000, 3)).astype(np.uint8)

lut[0,:] = lut[0,:]/10

lut_float = lut.astype(float)/256.0

def col_png(im):
    img = lut_float[im,:]
    return img

def angle(u):
    eigs, evec = la.eig([[u[0], u[1]], [u[1], u[2]]])
    v = evec[:,np.argmax(eigs)]
    if v[0]>0:
        return atan2(v[1], v[0])
    else:
        return atan2(-v[1], -v[0])



class TriangleMesh(object):
    def __init__(self, verts, tris):
        
        self.verts = np.asarray(verts, dtype=float)
        self.tris = tris
        self.Nv = len(tris)
        self.calculate_tri_centroids_areas()
        self.make_tri_neighbours()
        self.connectivity = self.make_tri_adjacency_matrix()
        self.connectivity_sums = np.squeeze(np.asarray(self.connectivity.sum(axis=1)))
        self.perimeters = self.make_tri_extended_matrix(2)
        self.perimeter_sums = np.squeeze(np.asarray(self.perimeters.sum(axis=1)))
        self.copy_regions = self.perimeters
        self.tri_data = {}
        self.tri_data['label'] = np.zeros((self.Nv,), dtype = np.int32)
        self.init_circular_cell()
        self.area = self.tri_area

    def get_label(self):
        return self.tri_data['label']

    def update_label(self, label):
        self.tri_data['label'] = label
    
        
    def init_circular_cell(self):
        tree = self.tri_centroid_tree
        l = tree.query_ball_point(np.array((50,50)), 20)
        self.tri_data['label'][l] = 1
            

    def calculate_tri_centroids_areas(self):
        self.tris = np.array(self.tris)
        tri_areas = []
        tri_centroids = []
        for p in self.tris:
            pl = self.verts[p,:]
            tri_centroids.append(centroid(pl))
            tri_areas.append(area(pl))
        self.tri_centroids = np.array(tri_centroids)
        self.tri_area = np.array(tri_areas)
        self.tri_centroid_tree = cKDTree(self.tri_centroids)

    def make_tri_neighbours(self):
        N = self.verts.shape[0]

        tris = np.asarray(self.tris)

        edges = (tris[:,[1,2,0]] + N*tris[:, [2,0,1]]).flatten()
        rev_edges = (tris[:, [2,0,1]] + N*tris[:, [1,2,0]]).flatten()
        
        sort_idx = np.argsort(edges)
        sorted_edges = edges[sort_idx]
        
        idx = np.searchsorted(sorted_edges, rev_edges)
        idx[idx==sort_idx.shape[0]] = 0

        m = sorted_edges[idx]!=rev_edges

        idx = sort_idx[idx]
        idx[m] = -1

        self.tri_neighbours = idx.reshape(-1,3)

    def make_tri_adjacency_matrix(self):
        # Specialized for triangles

        n = self.tris.shape[0]
        tri_edges = la.norm(self.verts[self.tris[:,[1,2,0]],:]-self.verts[self.tris[:,[2,0,1]],:], axis=2)

        i = np.repeat(np.arange(n),3)
        j = self.tri_neighbours/3
        j = j.flatten()
        data = tri_edges.flatten()
        idx = np.where(j>=0)[0]
        i = i[idx]
        j = j[idx]
        data = data[idx]
        print i.shape, j.shape, data.shape

        A = csr_matrix((data.flatten(), (i,j.flatten())), (n,n), dtype=np.float64)
        return A
            
    def make_poly_neighbour_matrix(self):
        # polygon-polygon immediate adjacency matrix (purely lengths ...)
        edge_map = {}
        adjacency_i = []
        adjacency_j = []
        adjacency_a = []
        for i, p in enumerate(self.tris):
            for (a, b) in pairs(p):
                if (b, a) in edge_map:
                    # Found pair
                    j = edge_map[(b,a)]
                    l = la.norm(self.verts[a,:] - self.verts[b,:])
                    adjacency_i.extend((i,j))
                    adjacency_j.extend((j,i))
                    adjacency_a.extend((l,l))
                else:
                    edge_map[(a,b)] = i
        matrix = coo_matrix((adjacency_a, (adjacency_i, adjacency_j))).tocsr()
        return matrix
        
    def make_tri_extended_matrix(self, r, r_tri_max=2.0):
        # Find maximum distance of a vertex from its centroid
        # TODO
        adjacency_i = []
        adjacency_j = []
        adjacency_a = []
        area = self.tri_area
        for i, p in enumerate(self.tris):
            nbs = self.tri_centroid_tree.query_ball_point(self.tri_centroids[i], r + r_tri_max)
            for j in nbs:
                if j!=i:
                    a = calc_tri_a(self.verts[self.tris[i], :], self.verts[self.tris[j], :], r)
                    if a>0.0:
#                        print i, j, a
#                        print calc_poly_a(self.verts[self.polys[i], :], self.verts[self.polys[j], :], r)
#                        print calc_poly_a4(self.verts[self.polys[i], :].T, self.verts[self.polys[j], :].T, r)
                        adjacency_i.append(i)
                        adjacency_j.append(j)
                        adjacency_a.append(a*area[i]*area[j])

        xi = 2*r**3/3.0
        matrix = coo_matrix((np.array(adjacency_a)/xi, (adjacency_i, adjacency_j))).tocsr()
        return matrix
            

    def draw_matplotlib(self, draw_centroid_neighbours=False):
#        fig = plt.figure()
        pl = []

        bb = np.max(self.verts, axis=0)
        r0 = 0.3*sqrt(bb[0]*bb[1]/len(self.verts)/pi)
                  
        # Draw polygons
        fc = []
        label = self.tri_data['label']
        cmap = np.array([[ 0.3, 0.3, 0.5], [0.4, 0.4, 0.9]])
        for j, p in enumerate(self.tris):
            tri = [self.verts[i] for i in p]
            pl.append(tri)
            fc.append(cmap[label[j],:])
#        pc = TriCollection(pl, facecolor=fc, lw=0.7, edgecolor=(0,0,0))
        pc = PolyCollection(pl, facecolor=fc, edgecolor='none')

        ax = plt.gca()
        
        ax.add_collection(pc, autolim=True)
        #ax.add_collection(lc, autolim=True)
        #ax.add_collection(cc, autolim=True)
        
        ax.autoscale_view()
        
        plt.draw()
        self.ax = ax
        self.pc = pc
        
    def update_matplotlib(self):
        fc = []
        label = self.tri_data['label']
        cmap = np.array([[ 0.3, 0.3, 0.5], [0.4, 0.4, 0.9]])
        for j, p in enumerate(self.tris):
            poly = [self.verts[i] for i in p]
            fc.append(cmap[label[j],:])

        self.pc.set_facecolor(fc)
        plt.draw()

    def calc_centroid(self):
        labels = self.tri_data['label']
        cell_area = np.sum(self.area[labels==1])
        cell_xa = np.sum(self.tri_centroids[labels==1,:]*self.tri_area[labels==1, np.newaxis], axis=0)
        return cell_xa / cell_area

    def calc_centroid_moments(self):
        labels = self.tri_data['label']
        a = self.tri_area[labels==1]
        cell_area = np.sum(a)
        c = self.tri_centroids[labels==1,:]
        cell_xa = np.sum(c*a[:, np.newaxis], axis=0)
        centroid = cell_xa / cell_area
        x = c - centroid[np.newaxis,:]
        moments  = (a[:,np.newaxis]*x).T.dot(x)
        return centroid, moments

    def calc_exact_centroid_moments(self):
        print self.tri_data
        labels = self.tri_data['label']
        a = self.tri_area[labels==1]
        cell_area = np.sum(a)
        c = self.tri_centroids[labels==1,:]
        cell_xa = np.sum(c*a[:, np.newaxis], axis=0)
        centroid = cell_xa / cell_area
        tri_pts = self.verts[self.tris[labels==1,:],:]
        x = tri_pts - centroid[np.newaxis,np.newaxis,:]
        M = np.array(( (2/12.0, 1/12.0, 1/12.0), (1/12.0, 2/12.0, 1/12.0), (1/12.0, 1/12.0, 2/12.0)))
        moments = np.einsum('i,ikj,kl,ilm->jm', a, x, M, x)
        return centroid, moments


class RegularTriangleMesh(TriangleMesh):
    def __init__(self, m, n, dm, dn):
        self.m = m
        self.n = n

        dm = np.asarray(dm)
        dn = np.asarray(dn)
        verts = np.array([i*dm+j*dn for i in range(m) for j in range(n)])
        tris = list(chain.from_iterable([[n*i+j,  n*(i+1)+j, n*i+j+1], [n*i+j+1,  n*(i+1)+j, n*(i+1)+j+1]] for i in range(m-1) for j in range(n-1)))
        TriangleMesh.__init__(self, verts, tris)
        print self.area

def reduce_matrix(mat, labels):
    mat = mat.tocoo()
    new_row = labels[mat.row]
    new_col = labels[mat.col]
    mask = new_row!=new_col
    new_row = new_row[mask]
    new_col = new_col[mask]
    new_data = mat.data[mask]
    return coo_matrix((new_data, (new_row, new_col))).tocsr()
        
class PolygonMesh(TriangleMesh):
    def __init__(self, verts, polys):
        self.verts = np.array(verts, dtype=np.float64)
        print self.verts.shape
        # check poly orientation
        new_polys = []
        for p in polys:
            if area(self.verts[p,:])<0:
                new_polys.append(list(reversed(p)))
            else:
                new_polys.append(p)

        self.polys = new_polys


        self.tris, self.tri_poly_num = self.make_tris()

        self.tri_data = {}
        self.poly_data = {}
        self.calculate_tri_centroids_areas()

        self.Np = len(self.polys)
        self.Nt = self.tris.shape[0]
        
        self.make_tri_neighbours()
        self.connectivity = self.make_poly_adjacency_matrix()
        self.connectivity_sums = np.squeeze(np.asarray(self.connectivity.sum(axis=1)))
        self.tri_perimeters = self.make_tri_extended_matrix(2)
        self.perimeters = reduce_matrix(self.tri_perimeters, self.tri_poly_num)
        # Now reduce these using tri_poly_num to give perimeter matrix
        self.perimeter_sums = np.squeeze(np.asarray(self.perimeters.sum(axis=1)))
        self.copy_regions = self.perimeters


        self.tri_data = {}
        self.poly_data['label'] = np.zeros((self.Np,), dtype = np.int32)
        self.area = nd.sum( self.tri_area, self.tri_poly_num, np.arange(self.Np))

        
        self.centroids = np.stack((nd.sum(self.tri_area*self.tri_centroids[:,0], self.tri_poly_num, index=np.arange(self.Np)),
                                    nd.sum( self.tri_area*self.tri_centroids[:,1], self.tri_poly_num, index=np.arange(self.Np)))).T/self.area[:,np.newaxis]
        self.centroid_tree = cKDTree(self.centroids)
        self.init_circular_cell()

        
    def init_circular_cell(self):
        tree = self.centroid_tree
        l = tree.query_ball_point(np.array((50,50)), 20)
        self.poly_data['label'][l] = 1
        self.tri_data['label'] = self.poly_data['label'][self.tri_poly_num]

    def get_label(self):
        return self.poly_data['label']
        
    def update_label(self, label):
        self.poly_data['label'] = label
        self.tri_data['label'] = self.poly_data['label'][self.tri_poly_num]
        
    def make_tris(self):
        tris = []
        tri_poly_num = []
        i = 0
        for p in self.polys:
            pp = self.verts[p,:]
            new_tris = ear_clip(pp)
            tris += [[p[j] for j in t] for t in new_tris]
            tri_poly_num += [i]*(len(p)-2)
            i+=1
            print len(new_tris), len(p)-2
        return np.array(tris), np.array(tri_poly_num)
            
    def make_poly_adjacency_matrix(self):
        # polygon-polygon immediate adjacency matrix (purely lengths ...)
        edge_map = {}
        adjacency_i = []
        adjacency_j = []
        adjacency_a = []
        for i, p in enumerate(self.polys):
            for (a, b) in pairs(p):
                if (b, a) in edge_map:
                    # Found pair
                    j = edge_map[(b,a)]
                    l = la.norm(self.verts[a,:] - self.verts[b,:])
                    adjacency_i.extend((i,j))
                    adjacency_j.extend((j,i))
                    adjacency_a.extend((l,l))
                else:
                    edge_map[(a,b)] = i
        matrix = coo_matrix((adjacency_a, (adjacency_i, adjacency_j))).tocsr()
        return matrix

    def draw_matplotlib(self, draw_centroid_neighbours=False):
#        fig = plt.figure()
        pl = []

        bb = np.max(self.verts, axis=0)
        r0 = 0.3*sqrt(bb[0]*bb[1]/len(self.verts)/pi)
                  
        # Draw polygons
        fc = []
        label = self.poly_data['label']
        cmap = np.array([[ 0.3, 0.3, 0.5], [0.4, 0.4, 0.9]])
        for j, p in enumerate(self.polys):
            poly = [self.verts[i] for i in p]
            pl.append(poly)
            fc.append(cmap[label[j],:])
#        pc = TriCollection(pl, facecolor=fc, lw=0.7, edgecolor=(0,0,0))
        pc = PolyCollection(pl, facecolor=fc, edgecolor='none')

        ax = plt.gca()
        
        ax.add_collection(pc, autolim=True)
        #ax.add_collection(lc, autolim=True)
        #ax.add_collection(cc, autolim=True)
        
        ax.autoscale_view()
        
        plt.draw()
        self.ax = ax
        self.pc = pc
        
    def update_matplotlib(self):
        fc = []
        label = self.poly_data['label']
        cmap = np.array([[ 0.3, 0.3, 0.5], [0.4, 0.4, 0.9]])
        for j, p in enumerate(self.polys):
            poly = [self.verts[i] for i in p]
            fc.append(cmap[label[j],:])

        self.pc.set_facecolor(fc)
        plt.draw()

class RectPolygonMesh(PolygonMesh):
    def __init__(self, m, n, dm, dn):
        self.m = m
        self.n = n

        dm = np.asarray(dm)
        dn = np.asarray(dn)
        verts = np.array([i*dm+j*dn for i in range(m) for j in range(n)])
        polys = [[n*i+j, n*i+j+1, n*(i+1)+j+1, n*(i+1)+j] for i in range(m-1) for j in range(n-1)]
        print verts
        print polys
        PolygonMesh.__init__(self, verts, polys)

    
plt.ion()
if __name__=="__main__":

    r = 2.0
    Sx = 100/sqrt(r)
    Sy = 100*sqrt(r)
    poly = [(0, 0), (Sx, 0), (Sx, Sy), (0, Sy)]
    h = 1.0

    v_list, lines, tris = mesh_polygon(poly, h*h)

    
    v_list = np.array(v_list)*np.array([sqrt(r),1./sqrt(r)])[np.newaxis,:]
    m = TriangleMesh(v_list, tris)
#    m = RectPolygonMesh(100, 100, np.array([1,0]), np.array([0,1]))
    print 'made_mesh'

    mCPM = MeshCPM(m, m.get_label())
    print 'init_perims', mCPM._perimeters
    print 'init_areas', mCPM._area
    print 'estimates', pi*20*20, 2*pi*20
    e0 = mCPM.calc_energy()
    print 'init energy', e0

    mCPM.update_area_perimeter()
    mCPM.SetTemperature(500)
    plt.figure()
    m.draw_matplotlib()
#    plt.show()
    centroids = []
    moments = []
    for i in range(500):
#        print m.calc_centroid_moments()
        c, mm = m.calc_exact_centroid_moments()
        centroids.append(c)
        moments.append(mm)
        mCPM.Run(100, multi=True)
        m.update_label(mCPM.state)
        #print 'e', mCPM.calc_energy()
        m.update_matplotlib()
        plt.pause(0.2)
    xx = [mm[0][0] for mm in moments]
    yy = [mm[1][1] for mm in moments]
    plt.ioff()
    plt.plot(xx)
    plt.plot(yy)
    plt.show()
