

import numpy as np
import scipy.ndimage as nd
import scipy.linalg as la
from algo import calc_mu
from scipy.spatial.distance import pdist, squareform
from numpy.linalg import svd, eig
from math import cos, sin, pi

def diameter(points):
    print points.shape
    D = pdist(points)
    D = squareform(D);
    d1, [i, j] = np.nanmax(D), np.unravel_index( np.argmax(D), D.shape )
    v = points[j,:] - points[i,:]
    return d1, v

def calc_diameters(points):
    d1, v1 = diameter(points)
    v1 = v1/la.norm(v1)
    
    projected_points = points - np.dot(points, v1)[:,np.newaxis]*v1[np.newaxis,:]
    d2, v2 = diameter(projected_points)

    return d1, d2, v1
    

    
def calculate_cell_diameters(verts, tris, ta, labels):

    v_array=np.asarray(verts) 
    tri_array=np.asarray(tris, dtype=int)
    tri_pts=v_array[tri_array]
    n = np.cross( tri_pts[:,1] - tri_pts[:,0], 
                  tri_pts[:,2] - tri_pts[:,0])
    
    # Triangle areas = 0.5 * | (v1 - v0) x (v2 - v0) |
    ta = 0.5*np.sqrt(n[:,0]**2+n[:,1]**2+n[:,2]**2)
    




    u, idx = np.unique(labels, return_inverse=True)

    d1 = {}
    d2 = {}
    v1 = {}
    for i in u:
        vl = verts[np.unique(tris[labels==i,:]), :].reshape(-1,3)
        d1[i], d2[i], v1[i] = calc_diameters(vl)


    return d1, d2, v1


#@profile
def calculate_cell_covariance(verts, tris, ta, labels):

    v_array=np.asarray(verts) 
    tri_array=np.asarray(tris, dtype=int)
    tri_pts=v_array[tri_array]
    n = np.cross( tri_pts[:,1] - tri_pts[:,0], 
                  tri_pts[:,2] - tri_pts[:,0])
    
    # Triangle areas = 0.5 * | (v1 - v0) x (v2 - v0) |
    ta = 0.5*np.sqrt(n[:,0]**2+n[:,1]**2+n[:,2]**2)

#    print 'area', ta[:10], ta2[:10], la.norm(ta-ta2)

#    print ta.shape, np.sum(tri_pts, axis=1).shape
    # Triangle area*centroid = area * (v0 + v1 + v2)/3

    print tri_pts[0,:,:], (np.sum(tri_pts, axis=1)/3.0)[0,:]
    
    
    tm = ta[:, np.newaxis]*np.sum(tri_pts, axis=1)/3.0

    
    # Triangle centroids

    tb = np.sum(tri_pts, axis=1)/3.0

    M = np.array(( (2/12.0, 1/12.0, 1/12.0), (1/12.0, 2/12.0, 1/12.0), (1/12.0, 1/12.0, 2/12.0)))
    nt = tri_array.shape[0]

    u, idx = np.unique(labels, return_inverse=True)

    
    ca_vec = np.bincount(idx, ta)

    
    cm_vec = np.zeros((ca_vec.shape[0], 3))
    for i in range(3):
        cm_vec[:,i] = np.bincount(idx, tm[:,i])

        
    cb_vec = np.nan_to_num(cm_vec/ca_vec[:,np.newaxis])

    cc_vec = np.zeros((ca_vec.shape[0], 3, 3))

    x = tri_pts - cb_vec[idx, np.newaxis, :]

        
    y = np.einsum('i,ikj,kl,ilm->ijm', ta, x, M, x)

    #   y = np.einsum('ijl,ilm->ijm', ta[:,np.newaxis,np.newaxis]*np.transpose(x,(0,2,1)).dot(M), x)


#    print 'accy', la.norm(y-y2)
    
    for i in range(3):
        for j in range(3):
            cc_vec[:,i,j] = np.bincount(idx, y[:,i,j])


    """
    ca = {}
    cm = {}
    for i, l in enumerate(labels):
        if l in ca:
            ca[l] += ta[i]
            cm[l] += tm[i,:]
        else:
            ca[l] = ta[i]
            cm[l] = np.array(tm[i,:])
    
    # Compute cell centroids
    cb = dict((l, cm[l]/ca[l] if ca[l]>0 else 0) for l in ca)

    cc = dict((l, np.zeros((3,3))) for l in ca)

    x = tri_pts - cb_vec[idx,:][:, np.newaxis, :]
    y = np.einsum('i,ikj,kl,ilm->ijm', ta, x, M, x)
    
    for i in range(3):
        for j in range(3):
            cc_vec[:,i,j] = np.bincount(idx, y[:,i,j])
    

    for i in range(nt):
        if ta[i]>0:
            l = labels[i]
            x = tri_pts[i,:,:] - cb[l][np.newaxis, :]
            tc = ta[i]*np.dot(x.T, np.dot(M, x))
            cc[l] += tc

    ## X = [x_0, x_1, x_2]
    ## cc = ta[i]* ((X-c)^T M (X-c))


    # Check results
    # ca

    print "check ca"
    for i, v in zip(u, ca_vec):
        print la.norm(v-ca[i])

    # cb 
    print "check cb"
    for i, v in zip(u, cb_vec):
        print la.norm(v-cb[i])


    # cc 
    print "check cc"
    for i, v in zip(u, cc_vec):
        print la.norm(v-cc[i])
    """

#    print ca_vec, cb_vec, cc_vec, u
    
    ca = dict(zip(u, ca_vec))
    cb = dict(zip(u, cb_vec))
    cc = dict(zip(u, cc_vec))

    """
    if 0 in ca:
        del ca[0]
        del cb[0]
        del cc[0]
    """ 

    return ca, cb, cc

def test():
    verts = [[ 0,0,0], [1,0,0],[1,1,0], [0, 1, 0]]#[[0.2, 0.3, 0.4], [1, 0.34, 1], [0.2, 1.5, 0.464]]
    tris = [[0, 1, 2], [0, 2, 3]]
    labels = [0, 0]
    
    ca, cb, cc =  calculate_cell_covariance(verts, tris, labels)

    print ca, cb, cc
    
    print calc_mu(cc[0]), ' = 0'

    verts = [[ 0.2,0.4,0], [2.2,0.4,0],[2.2,1.4,0], [0.2, 1.4, 0]]#[[0.2, 0.3, 0.4], [1, 0.34, 1], [0.2, 1.5, 0.464]]
    tris = [[0, 1, 2], [0, 2, 3]]
    labels = [0, 0]

    ca, cb, cc =  calculate_cell_covariance(verts, tris, labels)

    print calc_mu(cc[0]), ' = 1'


    verts += [[ 0,0,0], [1,0,0], [1,3,0], [0, 3, 0]]
    tris += [[4, 5, 6], [4, 6, 7]]
    labels += [1, 1]

    ca, cb, cc =  calculate_cell_covariance(verts, tris, labels)

    print calc_mu(cc[0]), ' = 1', calc_mu(cc[1]), ' = 2'


    # rotated and translated ellipse
    
    Nv = 10000
    c = np.array((3., 4., 1.))
    verts = [c]
    for i in range(Nv):
        verts.append(c+np.array((2*cos(i/100.*2*pi+0.2), sin(i/100.*2*pi+0.2), 0.)))
        tris.append([0, (i+1)%(Nv+1), (i+2)%(Nv+1)])
        labels.append(0)

    ca, cb, cc =  calculate_cell_covariance(verts, tris, labels)
    print calc_mu(cc[0]), ' = 1'


if __name__=="__main__":
    test()
