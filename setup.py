
from distutils.core import setup
from distutils.extension import Extension
from Cython.Build import cythonize
import numpy as np

ext_modules=[
    Extension("cpm_step_graph",
              sources=["cpm_step_graph.pyx", "pcg_basic.c"],
              extra_compile_args=['-O3', '-march=native', '--fast-math'],
              libraries=["m"], # Unix-like specific
              include_dirs = [np.get_include()],
    ),
    Extension("cpm_step_graph_multi",
              sources=["cpm_step_graph_multi.pyx", "pcg_basic.c"],
              extra_compile_args=['-O3', '-march=native', '--fast-math'],
              libraries=["m"], # Unix-like specific
              include_dirs = [np.get_include()],
    ),
    Extension("cpm_step_grid",
              sources=["cpm_step_grid.pyx", "pcg_basic.c"],
              extra_compile_args=['-O3', '-march=native', '--fast-math'],
              libraries=["m"], # Unix-like specific
              include_dirs = [np.get_include()],
    ),
    Extension("cpm_step_grid_multi",
              sources=["cpm_step_grid_multi.pyx", "pcg_basic.c"],
              extra_compile_args=['-O3', '-march=native', '--fast-math'],
              libraries=["m"], # Unix-like specific
              include_dirs = [np.get_include()],
    ),
    Extension("calc_tri",
              sources=["calc_tri.pyx"],
              extra_compile_args=['-O3', '-march=native', '--fast-math'],
              libraries=["m"], # Unix-like specific
              include_dirs = [np.get_include()],
    ),
    Extension("calc_tri3",
              sources=["calc_tri3.pyx"],
              extra_compile_args=['-O3', '-march=native', '--fast-math'],
              libraries=["m"], # Unix-like specific
              include_dirs = [np.get_include()],
    ),
]



setup(
  name = "cpm_step",
  ext_modules = cythonize(ext_modules)
)
