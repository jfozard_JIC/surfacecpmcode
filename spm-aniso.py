
import numpy as np
import scipy.ndimage as nd
import matplotlib.pyplot as plt
from PIL import Image
import scipy.ndimage as nd
from math import sqrt, ceil, floor, log, exp, atan2, pi
import warnings
from scipy.misc import imsave
import numpy.random as npr
import sys
from time import sleep
import numpy.linalg as la

from meshCPM import MeshCPM
from scipy.sparse import dok_matrix, csr_matrix
from cPickle import dump


nbs8 = [[-1,-1],[-1,0],[-1,1],[0,-1],[0,1],[1,-1],[1,0],[1,1]]
nbs20 = [[-2,-1],[-2,0],[-2,1],[-1,-2],[-1,-1],[-1,0],[-1,1],[-1,2], [0,-2], [0,-1],[0,1],[0,2],[1,-2],[1,-1],[1,0],[1,1],[1,2],[2,-1],[2,0],[2,1]]
nbs28 = [[-3,0],[-2,-2],[-2,-1],[-2,0],[-2,1],[-2,2],[-1,-2],[-1,-1],[-1,0],[-1,1],[-1,2],[0,-3],[0,-2],[0,-1],[0,1],[0,2],[0,3],[1,-2],[1,-1],[1,0],[1,1],[1,2],[2,-2],
         [2,-1],[2,0],[2,1],[2,2],[3,0]]

nbs4 = [[0,1],[1,0],[-1,0],[0,-1]]


lut = npr.randint(256, size=(20000, 3)).astype(np.uint8)

lut[0,:] = np.array([0,0,0])
lut[1,:] = np.array([245, 140, 160])

lut_float = lut.astype(float)/256.0

def save_png(im, fn):
    im2 = np.zeros((2*im.shape[0], 2*im.shape[1]), dtype=im.dtype)
    im2[::2,::2] = im
    im2[1::2,::2] = im
    im2[::2,1::2] = im
    im2[1::2,1::2] = im
    img = lut[im2,:]
    j = Image.fromarray(img)
    j.save(fn)

def col_png(im):
    img = lut_float[im,:]
    return img

def angle(u):
    eigs, evec = la.eig([[u[0], u[1]], [u[1], u[2]]])
    v = evec[:,np.argmax(eigs)]
    if v[0]>0:
        return atan2(v[1], v[0])
    else:
        return atan2(-v[1], -v[0])

def shape_covariance(im, sc):
    i, j = np.ogrid[:im.shape[0], :im.shape[1]]
    c = nd.center_of_mass(im)

    uii = nd.mean(sc[0]*sc[0]*(i-c[0])*(i-c[0]), im)
    uij = nd.mean(sc[1]*sc[0]*(i-c[0])*(j-c[1]), im)
    ujj = nd.mean(sc[1]*sc[1]*(j-c[1])*(j-c[1]), im)

    return uii, uij, ujj


def shape_covariance2(im, sc):
    i, j = np.ogrid[:im.shape[0], :im.shape[1]]
    c = nd.center_of_mass(im)
    
    uii = nd.mean(im*sc[0]*sc[0]*(i-c[0])*(i-c[0]), im)
    uij = nd.mean(im*sc[1]*sc[0]*(i-c[0])*(j-c[1]), im)
    ujj = nd.mean(im*sc[1]*sc[1]*(j-c[1])*(j-c[1]), im)

    return uii, uij, ujj




def simple_rect_perim(sp, R):
    max_di = int(ceil(float(R)/sp[0]))
    max_dj = int(ceil(float(R)/sp[1]))
    perim = np.zeros((max_di+1, max_dj+1))
    Ni = Nj = 1
    eps = (sp[0]*sp[1]/float(Ni*Nj))**2
    mi, mj = np.ogrid[:Ni, :Nj]
    grid_di = (mi[:,:,np.newaxis,np.newaxis] - mi[np.newaxis,np.newaxis,:,:])*float(sp[0])/Ni
    grid_dj = (mj[:,:,np.newaxis,np.newaxis] - mj[np.newaxis,np.newaxis,:,:])*float(sp[1])/Nj

    s_perim = 0.0
    for i in range(max_di+1):
        for j in range(max_dj+1):
            if i!=0 or j!=0:
                d2 = (grid_di+sp[0]*i)**2 + (grid_dj+sp[1]*j)**2
                perim[i,j] = np.sum(d2<R*R)*eps
                if i==0 or j==0:
                    s_perim += 2*perim[i,j]
                else:
                    s_perim +=4*perim[i,j]

    new_perim = []
    for i in range(-max_di, max_di+1):
        for j in range(-max_dj, max_dj+1):
            if (i!=0 or j!=0) and perim[abs(i), abs(j)]>1e-6:
                new_perim.append([[i,j],perim[abs(i), abs(j)]])
            #    print i, j, perim[abs(i), abs(j)]
    return new_perim


def exact_rect_perim(sp, R):
    max_di = int(ceil(float(R)/sp[0]))
    max_dj = int(ceil(float(R)/sp[1]))
    perim = np.zeros((max_di+1, max_dj+1))
    Ni = Nj = 100
    eps = (sp[0]*sp[1]/float(Ni*Nj))**2
    mi, mj = np.ogrid[:Ni, :Nj]
    grid_di = (mi[:,:,np.newaxis,np.newaxis] - mi[np.newaxis,np.newaxis,:,:])*float(sp[0])/Ni
    grid_dj = (mj[:,:,np.newaxis,np.newaxis] - mj[np.newaxis,np.newaxis,:,:])*float(sp[1])/Nj


    s_perim = 0.0
    for i in range(max_di+1):
        for j in range(max_dj+1):
            if i!=0 or j!=0:
                d2 = (grid_di+sp[0]*i)**2 + (grid_dj+sp[1]*j)**2
                perim[i,j] = np.sum(d2<R*R)*eps
                if i==0 or j==0:
                    s_perim += 2*perim[i,j]
                else:
                    s_perim += 4*perim[i,j]

    new_perim = []
    for i in range(-max_di, max_di+1):
        for j in range(-max_dj, max_dj+1):
            if i!=0 or j!=0:
                new_perim.append([[i,j],perim[abs(i), abs(j)]])

    return new_perim


"""
w_nbs = [[[0,1],spacing[0]],
         [[1,0],spacing[1]],
         [[-1,0],spacing[1]],
         [[0,-1],spacing[0]]]

"""

w_nbs = [[[0,1],1],
         [[1,0],1],
         [[-1,0],1],
         [[0,-1],1]]



def make_neighbourhoods(N, x_factor, y_factor):
    # Find r2 for circular neighborhood

    u = [ (i*i+j*j, i, j) for i in range(0, N+1) for j in range(0, N+1)]
    d = np.unique([x[0] for x in u])
    r2 = d[N+1]

    Ni = int(ceil(N/x_factor))
    Nj = int(ceil(N/y_factor))

    m = [ ((i,j), 1) for i in range(-Ni,Ni+1) for j in range(-Nj,Nj+1) if 0 < i*i*x_factor*x_factor+j*j*y_factor*y_factor <=r2]

    return m

#perim_nbs = make_neighbourhoods(6, sqrt(3), 1.0/sqrt(3))



class ImageMesh(object):
    def __init__(self, im, perim_nbs, w_nbs, eps):
        self.im = im
        self.Nv = im.shape[0]*im.shape[1]
        self.make_connectivity_matrix(im, perim_nbs, w_nbs, eps)

    def make_connectivity_matrix(self, im, perim_nbs, w_nbs, eps):

        print 'w_nbs', w_nbs
        print 'perim_nbs', perim_nbs

        n = self.Nv

        connections = {}
        edge_perimeters = {}

        for i in range(im.shape[0]):
            for j in range(im.shape[1]):
                a = i*im.shape[1]+j
                for (di, dj), w in w_nbs:
                    ni = (i+di)%im.shape[0]
                    nj = (j+dj)%im.shape[1]
                    if ni < 0:
                        ni += im.shape[0]
                    if nj < 0:
                        nj += im.shape[1]    
                    b = ni*im.shape[1]+nj
                    edge_perimeters[(a,b)] = w
                    connections[(a,b)] = w


        for i in range(im.shape[0]):
            for j in range(im.shape[1]):
                a = i*im.shape[1]+j
                for (di, dj), w in perim_nbs:
                    ni = (i+di)%im.shape[0]
                    nj = (j+dj)%im.shape[1]
                    if ni < 0:
                        ni += im.shape[0]
                    if nj < 0:
                        nj += im.shape[1]    
                    b = ni*im.shape[1]+nj
                    edge_perimeters[(a,b)] = w

                    
        A = dok_matrix((n,n), dtype=np.float64)
        A.update(connections)

        A = A.tocsr()
        print 'A check symmetric', (A - A.T).max(), (A - A.T).min()

        D = np.squeeze(np.asarray(A.sum(axis=1)))
        print D

        E = dok_matrix((n,n), dtype=np.float64)
        E.update(edge_perimeters)
        E = E.tocsr()
        ep = np.squeeze(np.asarray(E.sum(axis=1)))

        print 'E check symmetric', (E - E.T).max(), (E - E.T).min()

        self.connectivity = A
        self.connectivity_sums = D
        self.perimeters = E
        self.perimeter_sums = ep
        self.area = eps*np.ones((n,), dtype = np.float64)


    

def center_object(im):
    c = nd.center_of_mass(im)
    s = 0.5*np.array(im.shape) - np.array(c)

    return nd.shift(im.astype(float), s).astype(float)

#perim_nbs =  exact_rect_perim([1.0, 1.0], 2.0)


def analyse(data):
    t = range(1,len(data)/2)
    res = []
    for d in t:
        res.append(np.mean(np.abs(data[d:]-data[:-d])))
    return res



plt.ion()
if __name__=="__main__":
    import cPickle    

    print sys.argv

#    x_factor = sqrt(1.0)
#    y_factor = 1./x_factor
#    spacing = [x_factor, y_factor]

    spacing = [float(sys.argv[1]), float(sys.argv[2])]

    im = np.zeros((300, 300), dtype=np.uint8)

    ave_shape = np.zeros(nd.zoom(im, np.array(spacing)).shape)
    ave_occ = np.zeros(nd.zoom(im, np.array(spacing)).shape)
    n_shape = 0

    init_state = np.zeros(im.shape, dtype = np.int32)
    i, j = np.ogrid[:im.shape[0], :im.shape[1]]

    init_state = ((i-im.shape[0]/2)**2*spacing[0]*spacing[0] + (j-im.shape[1]/2)**2*spacing[1]*spacing[1]) < 30**2

    print ' init shape covariance', shape_covariance(init_state, spacing)

    w_nbs = [[u, 1.0] for u in nbs8]

    R = 2.5

    eps = spacing[0]*spacing[1]

    #perim_nbs = [(x,1) for x in nbs20]
    
    perim_nbs = exact_rect_perim(spacing, R)
 #   w_nbs = perim_nbs

#    perim_sc = float(sum(x*v for (x,y), v in perim_nbs if x>0))/spacing[0]
    perim_sc = 2/3.*R**3

    print 'perim_sc', perim_sc
    print 'exact perim', 120*pi
    print 'exact area', 60**2*pi

    new_perim_nbs = [(i,v/perim_sc) for i,v in perim_nbs]
    mesh = ImageMesh(im, new_perim_nbs, w_nbs, eps)


    data =[]
    data_u = []
    angles = []

    n_sims = 1
    n_step2 = 1000

    if len(sys.argv)>5:
        view = True
    else:
        view = False
    for i in range(n_sims):
        mCPM = MeshCPM(mesh, init_state.flatten())
        print 'init perims', mCPM._perimeters

        
        e0 = mCPM.calc_energy()
        print 'init energy', e0

        mCPM.update_area_perimeter()

        
        mCPM.nsteps = 100
        mCPM.SetTemperature(float(sys.argv[3]))
        data_run = []
        data_run_u = []
        for i in range(n_step2):
            dH = mCPM.Run(100)
            if i==0:
                e1 = mCPM.calc_energy()
                print 'energy change check', dH, e1-e0-dH

                p1 = mCPM._perimeters
                a1 = mCPM._area

                mCPM.update_area_perimeter()

                p2 = mCPM._perimeters
                a2 = mCPM._area

                print 'perimeter check', la.norm(p1 - p2)
                print 'area check', la.norm(a1 - a2)
                
            im_r = mCPM.state.reshape(im.shape)
            im_r2 = nd.zoom(im_r, np.array(spacing))

            save_png(im_r2, '/tmp/{}.png'.format(i))

            if view:
                plt.clf()
                plt.imshow(col_png(im_r2), interpolation='none')
                plt.draw()
                plt.pause(0.01)
            u = shape_covariance(im_r, spacing)
            print 'u', u
            data_run_u.append(u)
#            angles.append(angle(u))
            c = nd.center_of_mass(im_r)*np.array(spacing)
#            print c, nd.center_of_mass(im_r2)

            data_run.append(c)
            ave_shape += center_object(im_r2)
            ave_occ += im_r2
            n_shape +=1
            #plt.imshow(center_object(im_r2))
            #plt.draw()
            #plt.pause(0.01)
        data.append(data_run)
        data_u.append(data_run_u)

#    print shape_covariance2(ave_shape/n_shape, [spacing[0], spacing[1]])

    data = np.asarray(data)

    data_u = np.asarray(data_u)


    print 'data_u', data_u.shape

    print 'aniso', np.mean(data_u, axis=(0,1))

    x_diff = np.mean([analyse(data[i,:,0]) for i in range(data.shape[0])], axis=0)
    y_diff = np.mean([analyse(data[i,:,1]) for i in range(data.shape[0])], axis=0)

    print x_diff

    t = range(1, x_diff.shape[0]+1)

    ave_shape = ave_shape/n_shape
    ave_occ = ave_occ/n_shape

    dump({'data':data, 'ave_shape':ave_shape, 'ave_occ':ave_occ, 
          'w_nbs':w_nbs, 'perim_nbs':perim_nbs, 'spacing':spacing, 
          'temperature': mCPM.temperature, 'n_sims':n_sims,
          'n_step2':n_step2}, open(sys.argv[4], 'w'))
    

    g = np.mean(data_u, axis=0)
    ratio = g[:,0]/g[:,2]

    plt.ioff()
    plt.figure()
    plt.plot(ratio)
    plt.figure()
    plt.hist(angles, bins=100)
    plt.figure()
    plt.hist(angles)
    plt.figure()
    plt.imshow(ave_shape, cmap=plt.cm.inferno)
    plt.figure()
    plt.imshow(ave_occ, cmap=plt.cm.inferno)
    plt.figure()
    plt.plot(np.sqrt(t), x_diff)
    plt.hold(True)
    plt.plot(np.sqrt(t), y_diff)
    plt.show()
