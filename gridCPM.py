import numpy as np
import scipy.ndimage as nd
from math import sqrt, ceil, floor, log, exp
from random import randint, choice, random
import matplotlib.pyplot as plt
from cpm_step_grid import evolve_CPM as evolve_CPM
from cpm_step_grid import evolve_CPM_w
from cpm_step_grid_multi import evolve_CPM as evolve_CPM_multi
import numpy.random as npr
import numpy.linalg as la

def init_copyprob(temperature, dissipation):
    eps = 1e-16
    dhcutoff = 1e-9
    chance0 = int(ceil(-log(dhcutoff)*temperature-dissipation))
    chance1 = int(floor(-dissipation-eps))

    copyrange = chance0 - chance1 - 1
    copyprob = []
    for i in range(copyrange):
        copyprob.append(exp(-(i+chance1+1+dissipation)/float(temperature)))
    return chance0, chance1, np.array(copyprob)    

        
class GridCPM(object):

    def __init__(self, mesh, init_state):
        self.mesh = mesh
        self.Initialize(init_state)
        self.SetPars()

        
    def GetState(self):
        return self.state
        
    def Initialize(self, init_state):
        self.SetInitState(init_state)
        self.update_area_perimeter()
        self.reset_targets()

    def Run(self, nsteps=None, verbose=False, multi=False, fw=False):
        if nsteps == None:
            nsteps = self.nsteps
        r = 0
        for i in range(nsteps):
            r += self.evolve_CPM(self.state.shape[0]*self.state.shape[1], multi, fw)[3]            
            if i%10==0:
                print i, 'ncells', len(np.unique(self.state)), r
#                self.UpdatePersistance()
        print self._area, self._perimeters
        return r

    def SetParams(self, J11, lambda_area, lambda_perimeter):
        self.J = J11*np.array(((0.0, 1.0), (1.0, 1.0)))
        self.lambda_area = lambda_area
        self.lambda_perimeter = lambda_perimeter

    def SetPars(self):


        self.temperature = 100

        self.neighsize = np.sum(self.mesh.ep_w)
        print 'neighsize', self.neighsize
        self.pixelsize = self.mesh.area
        print 'pixelsize', self.pixelsize

        self.totalarea = self.mesh.area*self.state.shape[0]*self.state.shape[1]
        print 'totalarea', self.totalarea

        self.SetParams(1000, 0.0625, 0.077160)
        self.SetTemperature(100)

    def SetTemperature(self, temperature):
        self.temperature = temperature
#        self.chance0, self.chance1, self.copyprob = init_copyprob(self.temperature, 0.0)
#        print 'chance0, chance1, copyprob', self.chance0, self.chance1, self.copyprob
        


    def SetState(self, state):
        self.state = state.astype(np.int32)
        self.ncells = np.max(self.state)
        self.update_area_perimeter()

    def reset_targets(self):
        self.target_area = np.array(self._area, dtype='double')
        self.target_perimeter = np.array(self._perimeters, dtype='double')

    def set_targets(self):
        ta = 31416
        tp = 36000
        self.target_area = ta*np.ones(self._area.shape, dtype='double')
        self.target_perimeter = tp*np.ones(self._perimeters.shape, dtype='double')

    def SetInitState(self, init_state):
        self.state = np.array(init_state, dtype=np.int32)
        self.ncells = np.max(self.state)+1

    def evolve_CPM(self, n_steps, multi=False, fw=False):
        if not multi:
            if not fw:
                return evolve_CPM(self, n_steps)
            else:
                return evolve_CPM_w(self, n_steps)
        else:
            return evolve_CPM_multi(self, n_steps)
                    

    def calc_energy(self):
        N = self.ncells
        A = self.state
        va = self.mesh.area
        # Assume area and perimeter have already been updated.
        
        # Per cell terms
        H = self.lambda_area*np.sum((self._area[1:]-self.target_area[1:])**2) + \
            self.lambda_perimeter*np.sum((self._perimeters[1:]-self.target_perimeter[1:])**2)
        # Interfacial terms (slow)

        ep_i = self.mesh.ep_i
        ep_j = self.mesh.ep_j
        ep_w = self.mesh.ep_w
        
        for i in range(A.shape[0]):
            for j in range(A.shape[1]):
                s = A[i,j]
                nbs_s = A[(i + ep_i)%A.shape[0], (j + ep_j)%A.shape[1]]
                h = ep_w
                H += 0.5*np.dot(h*self.J[self.celltype[s], self.celltype[nbs_s]], nbs_s!=s)
        return H

                        
    def update_area_perimeter(self):
        N = self.ncells
        A = self.state
        va = self.mesh.area
        
        
        self.celltype = np.array([0] + [1]*(N-1), dtype=np.int32)

        self._area = va*np.bincount(A.flat, minlength=N).astype(np.float64)
        self._perimeters = np.zeros((N,)).astype(np.float64)
        
        # Explicit calculation using neighbours
        ep_i = self.mesh.ep_i
        ep_j = self.mesh.ep_j
        ep_w = self.mesh.ep_w

        for i in range(A.shape[0]):
            for j in range(A.shape[1]):
                s = A[i,j]
                nbs_s = A[(i + ep_i)%A.shape[0], (j + ep_j)%A.shape[1]]
                h = ep_w
                self._perimeters[s] += np.dot(h, nbs_s!=s)
